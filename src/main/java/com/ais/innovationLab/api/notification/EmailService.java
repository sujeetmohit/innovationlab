package com.ais.innovationLab.api.notification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.mail.internet.*;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

@Component
public class EmailService {

	@Value("${support.email.from}")
	private String from;

	@Value("${smtp.email.host}")
	private String host;

	@Value("${supportcc.email.to}")
	private String MailCCTo;

//	@Value("${app.ccEmailReminder}")
//	private String ccEmailReminder;

	@Autowired
	private JavaMailSender emailSender;

//	public void sendMail(String toEmail, String subject, String content, boolean isHTML) throws MessagingException {
//		MimeMessage msg = this.emailSender.createMimeMessage();
//		MimeMessageHelper helper = new MimeMessageHelper(msg, true);
//		helper.setTo(toEmail);
//		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
//		helper.setSubject(subject);
//		helper.setText(content, isHTML);
//		this.emailSender.send(msg);
//	}

	public void sendMail(String toEmail, String subject, String content, boolean isHTML)
			throws MessagingException, UnsupportedEncodingException {

		Properties properties = System.getProperties();

		properties.setProperty("mail.smtp.host", host);

		Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("Innovation.Lab@aisinfo.com", "*******");
			}
		});

		session.setDebug(true);
		MimeMessage msg = emailSender.createMimeMessage();
		InternetAddress fromAddress = new InternetAddress(from, "Buisness Transformation");
		MimeMessageHelper helper = new MimeMessageHelper(msg, true);
		try {
			helper.setTo(toEmail);
			helper.setFrom(fromAddress);
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
			helper.setSubject(subject);
			helper.setText(content, isHTML);
//			Transport.send(msg);
			this.emailSender.send(msg);
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}

	public void sendMailToMultiple(String toEmail, String subject, String content, boolean isHTML)
			throws MessagingException {
		MimeMessage msg = this.emailSender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(msg, true);
		helper.setTo(toEmail);
		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
		helper.setSubject(subject);
		helper.setText(content, isHTML);
		this.emailSender.send(msg);
	}
}
