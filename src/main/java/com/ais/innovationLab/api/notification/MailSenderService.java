package com.ais.innovationLab.api.notification;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@EnableAsync
public class MailSenderService {

	/**
	 * From address
	 */
	@Value("${support.email.from}")
	private String from;

	/**
	 * Support mail to
	 */
	@Value("${support.email.to}")
	private String MailTo;

	/**
	 * Java mail sender
	 */
	@Value("${supportcc.email.to}")
	private String MailCCTo;

	@Autowired
	private JavaMailSender mailSender;

	/**
	 * <b>Send to</b>
	 * <p>
	 * // * @param from Sender of the Mail
	 *
	 * // * @param to Receiver Of Mail // * @param subject Subject of the mail //s
	 * * @param msg Body of Mail
	 * 
	 * @return Success or Not
	 * @throws MessagingException           the messaging exception
	 * @throws UnsupportedEncodingException
	 */
	@Async
	public String sendTo(String name, String fileDetail, String toemail)
			throws MessagingException, UnsupportedEncodingException {

		String message = "";
		String msg = "Dear Sir / Madam \n " + "\n" + "\n" + "<p>Please find the attached offer letter for " + name
				+ "\n</p>" + "\n" + "\n" + "\n" + "<p>Please find Attachement details:\n</p>" +
//					"\n" +
//					"   <p>Name / Class  : " + project + "\n </p>" +
//					"   <p>Website / Method  : " + website + "\n </p>" +
//					"   <p>Exception : " + exception + "\n </p>" +
				"\n" + " \n" + " \n" + "\n" + "<p>Regards</p>\n" + "<p>AIS  TEAM</p>";

		String subject = "Offer Letter Approval for " + name;
		MimeMessage mimMessage = mailSender.createMimeMessage();
		InternetAddress fromAddress = new InternetAddress(from, "AIS-IT");
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(mimMessage, true);
			helper.setTo(toemail);
			helper.setSubject(subject);
			helper.setText(msg, true);
			helper.setFrom(fromAddress);
			helper.setCc(MailCCTo);
			FileSystemResource file = new FileSystemResource(fileDetail);
			helper.addAttachment(file.getFilename(), file);
			mailSender.send(mimMessage);
			log.info("Mail sent ");
		} catch (MessagingException e) {
			e.printStackTrace();
			log.error("Error in mailing service " + e.getMessage());
		}

		return message;
	}

	public String sendToWithApprovalLink(String name, String fileDetail, String toemail, String approvallink,
			String rejectedlink) throws MessagingException, UnsupportedEncodingException {

		String applink = "<a href=\"" + approvallink + "\">Approve Request</a>";
		String rejectlink = "<a href=\"" + rejectedlink + "\">Reject Request</a>";
		String message = "";
		String msg = "Dear Sir / Madam \n " + "\n" + "\n" + "<p>Please find the attached offer letter for " + name
				+ "\n</p>" + "\n" + "\n" + "\n" + "<p>Please find Attachement details:\n</p>" +
//				"\n" +
//				"   <p>Name / Class  : " + project + "\n </p>" +
//				"   <p>Website / Method  : " + website + "\n </p>" +
//				"   <p>Exception : " + exception + "\n </p>" +
				"\n" + " \n" + " \n" + "\n" + "Click on link " + applink + " to approve Or " + rejectlink
				+ " to reject offer letter request." + "\n" + "<p>Regards</p>\n" + "<p>AIS  TEAM</p>";

		String subject = "Offer Letter Approval for " + name;
		MimeMessage mimMessage = mailSender.createMimeMessage();
		InternetAddress fromAddress = new InternetAddress(from, "AIS-IT");
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(mimMessage, true);
			helper.setTo(toemail);
			helper.setSubject(subject);
			helper.setText(msg, true);
			helper.setFrom(fromAddress);
			helper.setCc(MailCCTo);
			FileSystemResource file = new FileSystemResource(fileDetail);
			helper.addAttachment(file.getFilename(), file);
			mailSender.send(mimMessage);
			log.info("Mail sent ");
			message = "sent";
		} catch (MessagingException e) {
			e.printStackTrace();
			log.error("Error in mailing service " + e.getMessage());
		}

		return message;
	}
}
