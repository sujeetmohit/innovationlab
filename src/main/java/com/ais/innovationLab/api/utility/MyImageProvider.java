package com.ais.innovationLab.api.utility;

import java.io.File;

import com.itextpdf.text.Image;
import com.itextpdf.tool.xml.pipeline.html.AbstractImageProvider;

public class MyImageProvider extends AbstractImageProvider {

	@Override
	public Image retrieve(final String src) {
		Image img = super.retrieve(src);
		if (img == null) {
			try {
				
			//	Object f1 =new File(src).toURI();
				img = com.itextpdf.text.Image.getInstance(new File(src).toURI().toURL());
//				 byte [] data = Base64.decode(src);
//				 img = Image.getInstance(data);
				super.store(src, img);
			} catch (Exception e) {
				// handle exceptions
			}
		}
		return img;
	}

	@Override
	public String getImageRootPath() {
//		return "http://localhost:4200/assets/img/brand/";
		return "src/main/resources/img/brand/";
	}
}
