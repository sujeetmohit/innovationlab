package com.ais.innovationLab.api.utility;

import java.text.Normalizer;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class isValid {

	public static boolean isNullOrEmpty(String inputString) {
		if (inputString != null && !inputString.trim().isEmpty())
			return false;
		return true;
	}

	public static String removeDiacritics(String inputString) {
		String nfdNormalizedString = Normalizer.normalize(inputString, Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(nfdNormalizedString).replaceAll("");
	}

	// BEGIN Validation for input char length

	public static boolean valid2Char(String inputString) {
		if (inputString.trim().length() > 2)
			return false;
		return true;
	}

	public static boolean valid4Char(String inputString) {
		if (inputString.trim().length() > 4)
			return false;
		return true;
	}

	public static boolean valid5Char(String inputString) {
		if (inputString.trim().length() > 5)
			return false;
		return true;
	}

	public static boolean valid9Char(String inputString) {
		if (inputString.trim().length() > 9)
			return false;
		return true;
	}

	public static boolean valid10Char(String inputString) {
		if (inputString.trim().length() > 10)
			return false;
		return true;
	}

	public static boolean valid15Char(String inputString) {
		if (inputString.trim().length() > 15)
			return false;
		return true;
	}

	public static boolean valid20Char(String inputString) {
		if (inputString.trim().length() > 20)
			return false;
		return true;
	}

	public static boolean valid25Char(String inputString) {
		if (inputString.trim().length() > 25)
			return false;
		return true;
	}

	public static boolean valid50Char(String inputString) {
		if (inputString.trim().length() > 50)
			return false;
		return true;
	}

	public static boolean valid100Char(String inputString) {
		if (inputString.trim().length() > 100)
			return false;
		return true;
	}

	public static boolean valid150Char(String inputString) {
		if (inputString.trim().length() > 150)
			return false;
		return true;
	}

	public static boolean valid200Char(String inputString) {
		if (inputString.trim().length() > 200)
			return false;
		return true;
	}

	public static boolean isValidEmail(String email) {
		String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		return Pattern.compile(EMAIL_STRING).matcher(email).matches();
	}

	public static boolean isValidIndianMobileNumber(String s) {
		Pattern p = Pattern.compile("^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$");
		Matcher m = p.matcher(s);
		return (m.find() && m.group().equals(s));
	}
	// END
}
