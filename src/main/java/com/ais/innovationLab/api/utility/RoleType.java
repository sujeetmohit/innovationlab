package com.ais.innovationLab.api.utility;

public enum RoleType {
	Manager, HR, CIO, President
}
