package com.ais.innovationLab.api.utility;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Value;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.XMLWorkerHelper;

public class HeaderFooterPageEvent extends PdfPageEventHelper {

	@Value("${app.offerLetterImage}")
	private String offerLetterImage;

	protected ElementList header;
	protected ElementList footer;
	public static final String HEADER = "<table width=\"100%\" border=\"0\"><tr><td>Header</td><td align=\"right\">Some title</td></tr></table>";
//	public static final String FOOTER = "<br/><br/><h4 style=\"text-align: center;\">AMERICAN INFOSOURCE BUSINESS SOLUTIONS PRIVATE LIMITED</h4><p style=\"text-align: center;font-size:14px;\">Office Address:<br />Office # 08-102,8th Floor,WeWork Blue One Square,246,Phase IV,Udyog Vihar,Gurugram Haryana -122016,India | ph. +91 1245025040.</p>";

	public HeaderFooterPageEvent(String officeAddress) throws IOException {
		String FOOTER = "<br/><br/><h5 style=\"text-align: center;\">AIS BUSINESS SOLUTIONS PRIVATE LIMITED</h5><p style=\"text-align: center;font-size:14px;\">Office Address:<br />"
				+ officeAddress.trim() + ".</p>";
		footer = XMLWorkerHelper.parseToElementList(FOOTER, null);
	}

	public void onStartPage(PdfWriter writer, Document document) {

//		String img = "http://localhost:4200/assets/img/brand/ais_header.jpg";
		String img = "http://10.1.10.138:8080/hris-portal/assets/img/brand/ais_header.jpg";

		try {
			Image image = Image.getInstance(img);
			PdfPTable tabHead = new PdfPTable(1);
			PdfPCell cell;
			tabHead.setWidthPercentage(100);
			cell = new PdfPCell(image, true);
			cell.setBorderWidth(0);
			tabHead.addCell(cell);
			tabHead.setSpacingAfter(20);
			document.add(tabHead);
		}

		catch (Exception e) {
			e.printStackTrace();
		}

		// ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER,
		// new Phrase(""), 300, 800, 0);
		// ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER,
		// new Phrase("Top Right"), 550, 800, 0);
	}

	public void onEndPage(PdfWriter writer, Document document) {
		try {
			ColumnText ct = new ColumnText(writer.getDirectContent());
			ct.setSimpleColumn(new Rectangle(36, 10, 559, 90));
			for (Element e : footer) {
				System.out.println("Element on footer: " + e.toString());
				ct.addElement(e);
			}
			ct.go();

		} catch (DocumentException de) {
			throw new ExceptionConverter(de);
		}
	}

//		ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER,
//				new Phrase("Office address will be here"), 110, 30, 0);
//		ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER,
//				new Phrase("page " + document.getPageNumber()), 550, 30, 0);

}
