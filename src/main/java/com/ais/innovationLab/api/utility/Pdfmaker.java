package com.ais.innovationLab.api.utility;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

public class Pdfmaker {
	@Autowired
	public static void savePdf(OutputStream out, String html, String officeAddress) {
		Document document = new Document(PageSize.A4, 20, 20, 2, 60);
		try {
			PdfWriter writer = PdfWriter.getInstance(document, out);
//			HeaderFooterPageEvent event = new HeaderFooterPageEvent();
			writer.setPageEvent(new HeaderFooterPageEvent(officeAddress));
			document.open();
			XMLWorkerHelper.getInstance().parseXHtml(writer, document, new StringReader(html));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			document.close();
		}
	}

	@Autowired
	public static ByteArrayInputStream previewPdf(ByteArrayOutputStream out, String html, String officeAddress) {
		Document document = new Document(PageSize.A4, 20, 20, 2, 60);
		try {
			PdfWriter writer = PdfWriter.getInstance(document, out);
//			HeaderFooterPageEvent event = new HeaderFooterPageEvent();
			writer.setPageEvent(new HeaderFooterPageEvent(officeAddress));
			document.open();
			XMLWorkerHelper.getInstance().parseXHtml(writer, document, new StringReader(html));

//			@SuppressWarnings("deprecation")
//			HTMLWorker htmlWorker = new HTMLWorker(document);
//			htmlWorker.parse(new StringReader(html));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			document.close();
		}
		return new ByteArrayInputStream(out.toByteArray());
	}

	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer) throws Exception {
		URL fileResource = FormPdfview.class.getResource("/templates");
		String html = FreemarkerUtils.loadFtlHtml(new File(fileResource.getFile()), "simpleForm.ftl", model);

		XMLWorkerHelper.getInstance().parseXHtml(writer, document, new ByteArrayInputStream(html.getBytes()),
				Charset.forName("UTF-8"));
	}
}
