package com.ais.innovationLab.api.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "Project_Type")
@Getter
@Setter
public class ResultType {
	@Id
	private int Id;
	private String resultareatype;
}
