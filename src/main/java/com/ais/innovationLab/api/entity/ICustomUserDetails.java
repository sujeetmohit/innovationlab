package com.ais.innovationLab.api.entity;

import org.springframework.security.core.userdetails.UserDetails;

public interface ICustomUserDetails extends UserDetails {
	public String getId();
}
