package com.ais.innovationLab.api.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "Lever_Technology")
@Getter
@Setter
public class LeverTechnology {
	
	@Id
	private int Id;
	private String technologyName;

}
