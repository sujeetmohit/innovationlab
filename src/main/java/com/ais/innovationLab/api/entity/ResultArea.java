package com.ais.innovationLab.api.entity;

import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity(name = "ResultArea")
@Getter
@Setter
public class ResultArea {

	@Id
	private int Id;
	private String resultareaName;

}
