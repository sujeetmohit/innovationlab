package com.ais.innovationLab.api.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "ProjectCharter")
@Getter
@Setter
public class ProjectCharter {

	@Id
	private int Id;
	private int projecttypeId;
	private int ideasubmissionId;
	private String projecttypeName;
	private String projectName;
	private int ProjectcharterstatusId;
	private String projectcharterstatusName;
	private String projectcharterstatusUpdatedby;
	private String statusupdatedDate;
	private String projectSponsor;
	private String projectManager;
	private String projectapprovalDate;
	private String lastrevisionDate;
	private String problemStatement;
	private String projectDescription;
	private String inScope;
	private String outScope;
	private int leverId;
	private String levertechName;
	private String projectDeliverable;
	private String buisnessCase;
	private String projectRisk;
	private String comments;
	private String createdBy;
	private String updatedBy;
	private String createdDate;
	private String updatedDate;

}
