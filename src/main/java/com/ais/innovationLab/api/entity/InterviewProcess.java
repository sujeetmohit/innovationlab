package com.ais.innovationLab.api.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class InterviewProcess {
	@Id
	private int id;
	private String interview_Date;
	//private String updateDate;
	@JsonProperty("interviewerName")
	private String interviewername;
	private String interview_Feedback;
	private int interview_Round;
	private int status;
	//private String NextRoundInverviewerName;
	private int applicant_Id;
	private String employee_No;
	private String interview_Type;
	private String totalinterviewround;
	@Column(name="final_round")
	private int isFinalRound;
	@JsonInclude()
	@Transient
	private String closedBy;
}
