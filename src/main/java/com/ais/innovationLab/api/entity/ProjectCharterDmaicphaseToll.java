package com.ais.innovationLab.api.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "ProjectCharterDmaicphaseToll")
public class ProjectCharterDmaicphaseToll {

	@Id
	private int Id;
	private int projectId;
	private int projecttypeId;
	private int dmaicId;
	private String dmailphaseName;
	private String startDate;
	private String endDate;
	private String tollgatereviewDate;
	private String createdBy;
	private String updatedBy;

}
