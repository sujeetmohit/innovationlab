package com.ais.innovationLab.api.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "Project_Status")
@Getter
@Setter
public class ProjectStatus {
	
	@Id
	private int Id;
	private String projectstatus_Name;

}
