package com.ais.innovationLab.api.entity;

import javax.persistence.Entity;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.Id;

@Getter
@Setter
@Entity(name = "ProjectCharterKeyStakeHolders")
public class ProjectCharterKeyStakeHolders {
	
	@Id
	private int Id;
	private int projectId;	
	private int projecttypeId;	
	private String ksName;	
	private String successCriteria;	
	private String createdBy;	
	private String updatedBy;	

}
