package com.ais.innovationLab.api.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class RolespecificDetail {

	@Id
	private String employeeId;
	private String employeeName;
	private String employeeEmail;

}
