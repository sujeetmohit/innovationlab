package com.ais.innovationLab.api.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class ReportProjectDashboard {

	@Id
	private int projectstatusId;
	private String projectstatusName;
	private int approved;
	private int rejected;
	private int returned;
	private int total;
}
