package com.ais.innovationLab.api.entity;

import java.util.ArrayList;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Report {

	private ArrayList<ReportIdeaDashboard> reportIdea;
	private ArrayList<ReportProjectDashboard> reportProject;
	
}
