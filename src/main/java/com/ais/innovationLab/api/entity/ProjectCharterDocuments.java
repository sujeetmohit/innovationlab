package com.ais.innovationLab.api.entity;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "ProjectCharterDocuments")
public class ProjectCharterDocuments {

	@Id
	private int Id;
	private int projectId;
	private int projecttypeId;
	private String documentName;
	private String documentNo;
	private String resp;
	private String requestedOn;
	private String updatedOn;
	private String respRepository;
	private String createdBy;
	private String updatedBy;

}
