package com.ais.innovationLab.api.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity(name = "DMAIC_Phase")
@Getter
@Setter
public class DMAIC_Phase {
	
	@Id
	private int Id;
	private String dmaicphaseName;

}
