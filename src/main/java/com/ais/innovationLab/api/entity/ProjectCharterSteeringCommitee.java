package com.ais.innovationLab.api.entity;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "ProjectCharterSteeringCommitee")
public class ProjectCharterSteeringCommitee {

	@Id
	private int Id;
	private int projectId;
	private int projecttypeId;
	private String champion;
	private String masterblackBelt;
	private String blackBelt;
	private String createdBy;
	private String updatedBy;

}
