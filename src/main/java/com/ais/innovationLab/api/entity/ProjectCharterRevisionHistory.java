package com.ais.innovationLab.api.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "ProjectCharterRevisionHistory")
public class ProjectCharterRevisionHistory {

	@Id
	private int Id;
	private int projectId;
	private int projecttypeId;
	private String description;
	private String dateModified;
	private String checkedBy;
	private String approvedBy;
	private String createdBy;
	private String updatedBy;

}
