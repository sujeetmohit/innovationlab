package com.ais.innovationLab.api.entity;

import org.springframework.web.multipart.MultipartFile;

public class FileModel {
	public FileModel() {

		super();

	}

	public FileModel(String name, String type, MultipartFile strm) {

		this.name = name;

		this.type = type;

		this.multipartFile = strm;

	}

	@SuppressWarnings("unused")
	private Long id;

	private String name;

	private String type;

	private MultipartFile multipartFile;

	public String getName() {

		return name;

	}

	public void setName(String name) {

		this.name = name;

	}

	public String getType() {

		return type;

	}

	public void setType(String type) {

		this.type = type;

	}

	public MultipartFile getMultipartFile() {
		return multipartFile;
	}

	public void setMultipartFile(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}

}
