package com.ais.innovationLab.api.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity(name = "ProjectCharterSteeringComtProjectTeam")
@Getter
@Setter
public class ProjectCharterSteeringComtProjectTeam {
	
	
	@Id
	private Integer Id;
	private Integer steeringId;
	private Integer projectId;
	private Integer projecttypeId;
	private String employeeId;
	private String employeeName;
	private String createdBy;
	private String updatedBy;

}
