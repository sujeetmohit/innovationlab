package com.ais.innovationLab.api.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "ProjectCharterBenefits")
public class ProjectCharterBenefits {
	@Id
	private Integer Id;
	private Integer projectId;
	private Integer projecttypeId;
	private String kpi;
	private String baseline;
	private String goal;
	private String createdBy;

}
