package com.ais.innovationLab.api.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "ProjectCharterResultArea")
public class ProjectCharterResultArea {
	
	@Id
	private int Id;
	private int projectId;
	private int projecttypeId;
	private int resultareaId;
	private String resultareaName;
	private String description;
	private String createdBy;
	private String updatedBy;

}
