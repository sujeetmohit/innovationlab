package com.ais.innovationLab.api.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "IdeaSubmission")
@Getter
@Setter
public class IdeaSubmission {

	@Id
	private int Id;
	private String ownerempid;
	private String ideasubmissionid;
	private String ownerofficialemail;
	private String ownerreportingmanageremail;
	private String ideaownername;
	private int projectstatusid;
	private String designationname;
	private int designationid;
	private String client;
	private String departmentname;
	private int departmentid;
	private String hod;
	private String reportingmanagerName;
	private String locationname;
	private int locationid;
	private String ideaproposalname;
	private String problemstatement;
	private int leverid;
	private String levertechnologyName;
	private String riskdescription;
	private String benefits;
	private String projectcommencementdate;
	private String projectcompletiondate;
	private int projecttypeid;
	private String projecttypeName;
	private String managerfeedback;
	private String receiptfilename;
	private int projectstatusidbymanager;
	private String projectstatusbyManager;
	private int projectstatusidbybtteam;
	private String projectstatusbyBtteam;
	private String btteamapprovalemail;
	private String createdby;
	private String createdbyName;
	private String updateby;
	private String createddate;
	private String updatedate;
}
