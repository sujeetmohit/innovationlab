package com.ais.innovationLab.api.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import com.ais.innovationLab.api.entity.IdeaSubmission;
import com.ais.innovationLab.api.entity.ProjectCharter;
import com.ais.innovationLab.api.notification.EmailService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@EnableAsync
public class EmailServiceHelper {

	@Value("${support.email.from}")
	private String from;

//	@Value("${api.url}")
//	private String apiUrl;

	/**
	 * Support mail to
	 */
	@Value("${support.email.to}")
	private String MailTo;

	/**
	 * Java mail sender
	 */
	@Value("${supportcc.email.to}")
	private String MailCCTo;

	@Value("${app.baseURL}")
	private String baseURL;

	@Value("${app.baseUIURL}")
	private String baseUIURL;

	@Value("${btTeamEmail}")
	public String btTeamEmail;

//	@Value("${app.ccEmailReminder}")
//	private String ccEmailReminder;

//	@Autowired
//	private JavaMailSender mailSender;

	@Autowired
	private EmailService emailService;

	private String approvelink = "<a href=\"" + "http://10.1.10.138:8080/innovationlab-portal/#/login" + "\">Login</a>";

	public int SendEmailUserForIdeaSubmission(IdeaSubmission submission, int id) throws Exception {
		int resultResponse = 0;
		try {
			StringBuilder buf = new StringBuilder();
			buf.append("<html>" + "<body>" + "Hello " + submission.getIdeaownername() + ",\n " + "\n" + "\n"
					+ "<p>Hope you are doing well ! \n</p>" + "\n" + "\n" + "\n"
					+ "<p>We would like to inform you that the idea (" + id + ") has been successfully"
					+ "submitted to the Innvation Lab portal.\n</p>"
					+ "<p>An Email has also been sent to Business Transformation team for approval." + "\n" + "\n</p>" +

					"<p>Kindly get in touch with our team (Business Transformation) in case of any "
					+ "discrepancy or queries." + "\n" + "\n</p>");

			buf.append("</table>\n" + "\n" + " \n" + " \n" +

					"\n" + " \n" + " \n" + "\n" + "<p>Thanks & Regards,<br>" + "Business Transformation<br>"
					+ "businesstransformation@aisinfo.com\r\n" + "<p>" + approvelink + "\n</p>" + "" + "</p>"
					+ "</body>" + "</html>");

			String html = buf.toString();
			String subject = "Idea Submission - " + submission.getIdeaproposalname();
			this.emailService.sendMail(submission.getOwnerofficialemail(), subject, html, true);
			resultResponse = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in EmailServiceHelper method SendEmailUserForIdeaSubmission(): "
					+ e.getMessage());
			resultResponse = -1;
			throw new Exception("SendEmailUserForIdeaSubmission failed.");
		}
		return resultResponse;
	}

	public int SendEmailBTForIdeaSubmission(IdeaSubmission submission, int id) throws Exception {
		int resultResponse = 0;
		try {
			StringBuilder buf = new StringBuilder();

			buf.append("<html>" + "<body>" + "Hello " + "BT Team" + ",\n " + "\n" + "\n"
					+ "<p>Hope you are doing well !! \n</p>" + "\n" + "\n" + "\n"
					+ "<p>We would like to inform you that the idea (" + id + ") has been successfully "
					+ " submitted to the Innvation Lab portal.\n</p>"
					+ "<p>Kindly review the submitted idea based on the overall impact on the project." + "\n"
					+ "\n</p>");
			buf.append("<p>Kindly get in touch with our team(Business Transformation) in case of thany "
					+ "discrepancy or queries."
					+ "<p>Please login to the Innovation Lab portal and take the required action.</p>" + "\n" + "<p>"
					+ approvelink + "\n</p>" + "\n" + "\n</p>");

			buf.append("</table>\n" + "\n" + " \n" + " \n" +

					"\n" + " \n" + " \n" + "\n" + "<p>Thanks & Regards<br>" + "Business Transformation<br>"
					+ "businesstransformation@aisinfo.com\r\n" + "" + "</p>" + "</body>" + "</html>");

			String html = buf.toString();
			String subject = "Required IdeaSubmission Approval - " + submission.getIdeaproposalname();

			this.emailService.sendMail(this.btTeamEmail, subject, html, true);

			resultResponse = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in EmailServiceHelper method SendEmailBTForIdeaSubmission(): "
					+ e.getMessage());
			resultResponse = -1;
			throw new Exception("SendEmailBTForIdeaSubmission failed.");
		}
		return resultResponse;
	}

	public int SendEmailAfterBTApproveIdea(IdeaSubmission submission, int id) throws Exception {
		int resultResponse = 0;
		try {
			StringBuilder buf = new StringBuilder();
			buf.append("<html>" + "<body>" + "Hello " + submission.getIdeaownername() + ",\n " + "\n" + "\n"
					+ "<p>Hope you are doing well !! \n</p>" + "\n" + "\n" + "\n"
					+ "<p>We would like to inform you that the idea (" + submission.getId() + ") has been successfully "
					+ "approved by Business Transformation in the Innovation Lab portal.\n</p>"
					+ "<p>The next step in the process is to submit the Project Charter available in the Innovation Lab portal. "
					+ "Once filled in all the required fields and submitted in the portal, "
					+ "it will be routed to the Business Transformation team for review and approval." + "\n" + "\n</p>"
					+

					"<p><mark>Kindly get in touch with our team(Business Transformation) in case of any "
					+ "discrepancy or queries." + "\n" + "\n</mark></p>");

			buf.append("</table>\n" + "\n" + " \n" + " \n" + "<tr></tr>" +

					"\n" + " \n" + " \n" + "\n" + "<p>Thanks & Regards<br>" + "Business Transformation<br>"
					+ "businesstransformation@aisinfo.com\r\n" + "<p>" + approvelink + "\n</p>" + "" + "</p>"
					+ "</body>" + "</html>");

			String html = buf.toString();
			String subject = "Idea Approved - " + submission.getIdeaownername();

			this.emailService.sendMail(submission.getCreatedby(), subject, html, true);

			resultResponse = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in EmailServiceHelper method SendEmailAfterBTApproveIdea(): "
					+ e.getMessage());
			resultResponse = -1;
			throw new Exception("SendEmailAfterBTApproveIdea failed.");
		}
		return resultResponse;
	}

	public int SendEmailUserToProjectCharter(ProjectCharter submission, int id, IdeaSubmission ideaSubmissionDetails)
			throws Exception {
		int resultResponse = 0;
		try {
			StringBuilder buf = new StringBuilder();
			buf.append("<html>" + "<body>" + "Hello " + ideaSubmissionDetails.getIdeaownername() + ",\n " + "\n" + "\n"
//			buf.append("<html>" + "<body>" + "Hello " + "\n" + "\n" + "<p>Hope you are doing well !! \n</p>" + "\n"
					+ "\n" + "\n" + "<p>We would like to inform you that the Project Charter for idea ("
					+ ideaSubmissionDetails.getIdeasubmissionid()
					+ ") has been successfully submitted to the Innovation Lab portal.\n</p>"
					+ "<p>An Email has also been sent to Business Transformation team for approval" + "\n" + "\n</p>" +

					"<p>Kindly get in touch with our team(Business Transformation) in case of any "
					+ "discrepancy or queries." + "\n" + "\n</p>");

			buf.append("</table>\n" + "\n" + " \n" + " \n" + "<tr></tr>" + "\n" + " \n" + " \n" + "\n"
					+ "<p>Thanks & Regards<br>" + "Business Transformation<br>"
					+ "businesstransformation@aisinfo.com\r\n" + "<p>" + approvelink + "\n</p>" + "" + "</p>"
					+ "</body>" + "</html>");

			String html = buf.toString();
			String subject = "Project Charter Submission -" + submission.getProjectName();
			this.emailService.sendMail(submission.getCreatedBy(), subject, html, true);
			resultResponse = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in EmailServiceHelper method SendEmailUserToProjectCharter(): "
					+ e.getMessage());
			resultResponse = -1;
			throw new Exception("SendEmailUserToProjectCharter failed.");
		}
		return resultResponse;
	}

	public int SendEmailBTForProjectCharter(ProjectCharter submission, int id, IdeaSubmission ideaSubmissionDetails)
			throws Exception {
		int resultResponse = 0;
		try {
			StringBuilder buf = new StringBuilder();
			buf.append("<html>" + "<body>" + "Hello " + "BT Team" + ",\n " + "\n" + "\n"
					+ "<p>Hope you are doing well !! \n</p>" + "\n" + "\n" + "\n"
					+ "<p>We would like to inform you that the idea (" + ideaSubmissionDetails.getIdeasubmissionid()
					+ ") by (" + submission.getProjectName() + ") has been successfully"
					+ "submitted to the Innvation Lab portal\n</p>"
					+ "<p>Kindly review the submitted project charter based on the overall impact on the project."
					+ "\n" + "\n</p>");
			buf.append("<p>Kindly get in touch with our team(Business Transformation) in case of any "
					+ "discrepancy or queries." + "</br>"
					+ "Please login to the Innovation Lab portal and take the required action." + "\n" + "<p>"
					+ approvelink + "\n</p>" + "\n" + "\n</p>");

			buf.append("</table>\n" + "\n" + " \n" + " \n" +

					"\n" + " \n" + " \n" + "\n" + "<p>Thanks & Regards<br>" + "Business Transformation<br>"
					+ "businesstransformation@aisinfo.com\r\n" + "" + "</p>" + "</body>" + "</html>");

			String html = buf.toString();
			String subject = "Required Project Charter Approval -" + submission.getProjectName();
			this.emailService.sendMail(this.btTeamEmail, subject, html, true);
			resultResponse = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in EmailServiceHelper method SendEmailBTForProjectCharter(): "
					+ e.getMessage());
			resultResponse = -1;
			throw new Exception("SendEmailBTForProjectCharter failed.");
		}
		return resultResponse;
	}

	public int SendEmailAfterBTApproveProject(ProjectCharter projectCharter, int id,
			IdeaSubmission ideaSubmissionDetails) throws Exception {
		int resultResponse = 0;
		try {
			StringBuilder buf = new StringBuilder();
			buf.append("<html>" + "<body>" + "Hello " + ideaSubmissionDetails.getIdeaownername() + ",\n " + "\n" + "\n"
					+ "<p>Hope you are doing well !! \n</p>" + "\n" + "\n" + "\n"
					+ "<p>We would like to inform you that the idea (" + projectCharter.getIdeasubmissionId()
					+ ") has been successfully"
					+ "approved by Business Transformation in the Innovation Lab portal.\n</p>"
					+ "<p>The next step in the process is to submit the Project Charter available in the Innovation Lab portal. "
					+ "Once filled in all the required fields and submitted in the portal, "
					+ "it will be routed to the Business Transformation team for review and approval." + "\n" + "\n</p>"
					+

					"<p><mark>Kindly get in touch with our team(Business Transformation) in case of any "
					+ "discrepancy or queries." + "\n" + "\n</mark></p>");

			buf.append("</table>\n" + "\n" + " \n" + " \n" + "<tr></tr>" +

					"\n" + " \n" + " \n" + "\n" + "<p>Thanks & Regards<br>" + "Business Transformation<br>"
					+ "businesstransformation@aisinfo.com\r\n" + "<p>" + approvelink + "\n</p>" + "" + "</p>"
					+ "</body>" + "</html>");

			String html = buf.toString();
			String subject = "Project Charter Approved - " + projectCharter.getProjectName();
			this.emailService.sendMail(projectCharter.getCreatedBy(), subject, html, true);
			resultResponse = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in EmailServiceHelper method SendEmailAfterBTApproveProject(): "
					+ e.getMessage());
			resultResponse = -1;
			throw new Exception("SendEmailAfterBTApproveProject failed.");
		}
		return resultResponse;
	}

	public int SendEmailUserForRejectIdea(IdeaSubmission submission, int id) throws Exception {
		int resultResponse = 0;
		try {
			StringBuilder buf = new StringBuilder();
			buf.append("<html>" + "<body>" + "Hello " + submission.getIdeaownername() + ",\n " + "\n" + "\n"
					+ "<p>Hope you are doing well !! \n</p>" + "\n" + "\n" + "\n"
					+ "<p>We would like to inform you that the idea (" + id + ") has been rejected"
					+ "by Business Transformation team in the Innvation Lab portal.\n</p>"
					+ "<p>The comment has also been added by Business Transformation stating the reason for the rejection."
					+ "\n" + "\n</p>" +

					"<p><mark>Kindly get in touch with our team(Business Transformation) in case of any "
					+ "discrepancy or queries." + "\n" + "\n</mark></p>");

			buf.append("</table>\n" + "\n" + " \n" + " \n" +

					"\n" + " \n" + " \n" + "\n" + "<p>Thanks & Regards<br>" + "Business Transformation<br>"
					+ "businesstransformation@aisinfo.com\r\n" + "<p>" + approvelink + "\n</p>" + "" + "</p>"
					+ "</body>" + "</html>");

			String html = buf.toString();
			String subject = "Idea Submission Rejected - " + submission.getIdeaproposalname();
			this.emailService.sendMail(submission.getCreatedby(), subject, html, true);
			resultResponse = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in EmailServiceHelper method SendEmailUserForRejectIdea(): "
					+ e.getMessage());
			resultResponse = -1;
			throw new Exception("SendEmailUserForRejectIdea failed.");
		}
		return resultResponse;
	}

	public int SendEmailUserForRejectProject(ProjectCharter projectCharter, int id,
			IdeaSubmission ideaSubmissionDetails) throws Exception {
		int resultResponse = 0;
		try {
			StringBuilder buf = new StringBuilder();
			buf.append("<html>" + "<body>" + "Hello " + ideaSubmissionDetails.getIdeaownername() + ",\n " + "\n" + "\n"
					+ "<p>Hope you are doing well !! \n</p>" + "\n" + "\n" + "\n"
					+ "<p>We would like to inform you that the Project Charter for idea ("
					+ ideaSubmissionDetails.getIdeasubmissionid()
					+ ") has been rejected by Business Transformation team in the Innvation Lab portal.\n</p>"
					+ "<p>The comment has also been added by Business Transformation stating the reason for the rejection."
					+ "\n" + "\n</p>" +

					"<p>Kindly get in touch with our team(Business Transformation) in case of any "
					+ "discrepancy or queries." + "\n" + "\n</p>");

			buf.append("</table>\n" + "\n" + " \n" + " \n" + "<tr></tr>" + "\n" + " \n" + " \n" + "\n"
					+ "<p>Thanks & Regards<br>" + "Business Transformation<br>"
					+ "businesstransformation@aisinfo.com\r\n" + "<p>" + approvelink + "\n</p>" + "" + "</p>"
					+ "</body>" + "</html>");

			String html = buf.toString();
			String subject = "Project Charter Submission -" + "add project name";
			this.emailService.sendMail(projectCharter.getCreatedBy(), subject, html, true);
			resultResponse = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in EmailServiceHelper method SendEmailUserForRejectProject(): "
					+ e.getMessage());
			resultResponse = -1;
			throw new Exception("SendEmailUserForRejectProject failed.");
		}
		return resultResponse;
	}

	public int SendEmailMangerForRejectProject(IdeaSubmission submission, int id) throws Exception {
		int resultResponse = 0;
		try {
			StringBuilder buf = new StringBuilder();
			buf.append("<html>" + "<body>" + "Hello " + submission.getReportingmanagerName() + ",\n " + "\n" + "\n"
					+ "<p>Hope you are doing well !! \n</p>" + "\n" + "\n" + "\n"
					+ "<p>We would like to inform you that the Project Charter for idea (" + submission.getId() + ") ("
					+ "(" + submission.getIdeaownername()
					+ ") has been rejected by Business Transformation team in the Innvation Lab portal.\n</p>"
					+ "<p>The comment has also been added by Business Transformation stating the reason for the rejection."
					+ "\n" + "\n</p>" +

					"<p>Kindly get in touch with our team(Business Transformation) in case of any "
					+ "discrepancy or queries." + "\n" + "\n</p>");

			buf.append("</table>\n" + "\n" + " \n" + " \n" + "<tr></tr>" + "\n" + " \n" + " \n" + "\n"
					+ "<p>Thanks & Regards<br>" + "Business Transformation<br>"
					+ "businesstransformation@aisinfo.com\r\n" + "<p>" + approvelink + "\n</p>" + "" + "</p>"
					+ "</body>" + "</html>");

			String html = buf.toString();
			String subject = "Project Charter Submission -" + "add project name";
			this.emailService.sendMail(submission.getOwnerreportingmanageremail(), subject, html, true);
			resultResponse = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in EmailServiceHelper method SendEmailMangerForRejectProject(): "
					+ e.getMessage());
			resultResponse = -1;
			throw new Exception("SendEmailMangerForRejectProject failed.");
		}
		return resultResponse;
	}

	public int SendEmailUserForIdeaReturn(IdeaSubmission submission, int id) throws Exception {
		int resultResponse = 0;
		try {
			StringBuilder buf = new StringBuilder();
			buf.append("<html>" + "<body>" + "Hello " + submission.getIdeaownername() + ",\n " + "\n" + "\n"
					+ "<p>Hope you are doing well !! \n</p>" + "\n" + "\n" + "\n"
					+ "<p>We would like to inform you that the idea " + id + " has been returned "
					+ "by Business Transformation team in the Innvation Lab portal\n</p>"
					+ "<p>The comment has also been added by the team stating the reason for the return. "
					+ "Requesting you to acknowledge the changes mentioned in the comments. " + "\n" + "\n</p>" +

					"<p><mark>Kindly get in touch with our team(Business Transformation) in case of any "
					+ "discrepancy or queries." + "\n" + "\n</mark></p>");

			buf.append("</table>\n" + "\n" + " \n" + " \n" +

					"\n" + " \n" + " \n" + "\n" + "<p>Thanks & Regards<br>" + "Business Transformation<br>"
					+ "businesstransformation@aisinfo.com\r\n" + "<p>" + approvelink + "\n</p>" + "" + "</p>"
					+ "</body>" + "</html>");

			String html = buf.toString();
			String subject = "Idea Submission Returned - " + submission.getIdeaproposalname();
			this.emailService.sendMail(submission.getOwnerofficialemail(), subject, html, true);
			resultResponse = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in EmailServiceHelper method SendEmailUserForIdeaReturn(): "
					+ e.getMessage());
			resultResponse = -1;
			throw new Exception("SendEmailUserForIdeaReturn failed.");
		}
		return resultResponse;
	}

	public int SendEmailUserToProjectCharterReturn(ProjectCharter submission, int id,
			IdeaSubmission ideaSubmissionDetails) throws Exception {
		int resultResponse = 0;
		try {
			StringBuilder buf = new StringBuilder();
			buf.append("<html>" + "<body>" + "Hello " + ideaSubmissionDetails.getIdeaownername() + ",\n " + "\n" + "\n"
					+ "<p>Hope you are doing well !! \n</p>" + "\n" + "\n" + "\n"
					+ "<p>We would like to inform you that the Project Charter for idea ("
					+ ideaSubmissionDetails.getId() + ") \n (" + "add idea ownername"
					+ ") has been successfully submitted to the Innovation Lab portal.\n</p>"
					+ "<p>An Email has also been sent to Business Transformation team for approval" + "\n" + "\n</p>" +

					"<p>Kindly get in touch with our team(Business Transformation) in case of any "
					+ "discrepancy or queries." + "\n" + "\n</p>");

			buf.append("</table>\n" + "\n" + " \n" + " \n" + "<tr></tr>" + "\n" + " \n" + " \n" + "\n"
					+ "<p>Thanks & Regards<br>" + "Business Transformation<br>"
					+ "businesstransformation@aisinfo.com\r\n" + "<p>" + approvelink + "\n</p>" + "" + "</p>"
					+ "</body>" + "</html>");

			String html = buf.toString();
			String subject = "Project Charter Returned -" + submission.getProjectName();
			this.emailService.sendMail(submission.getCreatedBy(), subject, html, true);
			resultResponse = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in EmailServiceHelper method SendEmailUserToProjectCharterReturn(): "
							+ e.getMessage());
			resultResponse = -1;
			throw new Exception("SendEmailUserToProjectCharterReturn failed.");
		}
		return resultResponse;
	}

	public int SendEmailUserForCompletion(IdeaSubmission submission, int id) throws Exception {
		int resultResponse = 0;
		try {
			StringBuilder buf = new StringBuilder();
			buf.append("<html>" + "<body>" + "Hello " + submission.getIdeaownername() + ",\n " + "\n" + "\n"
					+ "<p>Hope you are doing well !! \n</p>" + "\n" + "\n" + "\n"
					+ "<p>Congratulations! We would like to inform you that the Idea (" + id
					+ ") is now completed. \n</p>"
					+ "<p>Your project completion certificate will be unlocked shortly on the portal.\r\n" + "" + "\n"
					+ "\n</p>" +

					"<p><mark>Kindly get in touch with our team(Business Transformation) in case of any "
					+ "discrepancy or queries." + "\n" + "\n</mark></p>");

			buf.append("</table>\n" + "\n" + " \n" + " \n" +

					"\n" + " \n" + " \n" + "\n" + "<p>Thanks & Regards<br>" + "Business Transformation<br>"
					+ "businesstransformation@aisinfo.com\r\n" + "<p>" + approvelink + "\n</p>" + "" + "</p>"
					+ "</body>" + "</html>");

			String html = buf.toString();
			String subject = "Idea Submission - " + submission.getIdeaproposalname();
			this.emailService.sendMail(submission.getOwnerofficialemail(), subject, html, true);
			resultResponse = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in EmailServiceHelper method SendEmailUserForCompletion(): "
					+ e.getMessage());
			resultResponse = -1;
			throw new Exception("SendEmailUserForCompletion failed.");
		}
		return resultResponse;
	}

}
