package com.ais.innovationLab.api.service;

import org.springframework.stereotype.Service;
import com.ais.innovationLab.api.entity.Report;
import com.ais.innovationLab.api.entity.ResponseStatus;

@Service
public interface ReportService {

	public ResponseStatus<Report> GetDashBoardReportService() throws Exception;

}
