package com.ais.innovationLab.api.service;

import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ais.innovationLab.api.entity.CustomUser;
import com.ais.innovationLab.api.entity.ICustomUserDetails;
import com.ais.innovationLab.api.entity.User;
import com.ais.innovationLab.api.exceptions.UserNotActiveException;
import com.ais.innovationLab.api.repository.UserRepository;

@Service
@Transactional
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public ICustomUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = this.userRepository.findByEmail(username)
				.orElseThrow(() -> new UsernameNotFoundException("Email " + username + " not found"));
		if(!user.isEnabled()) {
			throw new UserNotActiveException("User is not active");
		}
		return new CustomUser(user.getEmail(), user.getPassword(),
		         getAuthorities(user),user.getId());
	}

	private static Collection<? extends GrantedAuthority> getAuthorities(User user) {
		String[] userRoles = user.getRoles().stream().map((role) -> role.getName()).toArray(String[]::new);
		Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(userRoles);
		return authorities;
	}

}
