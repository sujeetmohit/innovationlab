package com.ais.innovationLab.api.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ais.innovationLab.api.entity.IdeaSubmission;
import com.ais.innovationLab.api.entity.ProjectCharter;
import com.ais.innovationLab.api.entity.ProjectCharterBenefits;
import com.ais.innovationLab.api.entity.ProjectCharterDmaicphaseToll;
import com.ais.innovationLab.api.entity.ProjectCharterDocuments;
import com.ais.innovationLab.api.entity.ProjectCharterKeyStakeHolders;
import com.ais.innovationLab.api.entity.ProjectCharterResultArea;
import com.ais.innovationLab.api.entity.ProjectCharterRevisionHistory;
import com.ais.innovationLab.api.entity.ProjectCharterSteeringCommitee;
import com.ais.innovationLab.api.entity.ProjectCharterSteeringComtProjectTeam;
import com.ais.innovationLab.api.entity.ResponseStatus;
import com.ais.innovationLab.api.helper.EmailServiceHelper;
import com.ais.innovationLab.api.repository.IdeaSubmissionRepository;
import com.ais.innovationLab.api.repository.ProjectCharterRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ProjectCharterServiceImpl implements ProjectCharterService {

	@Autowired
	private ProjectCharterRepository projectCharterRepository;

	@Autowired
	private IdeaSubmissionRepository ideaSubmissionRepository;

	@Autowired
	private EmailServiceHelper serviceHelper;

	public ResponseStatus<List<ProjectCharter>> GetProjectCharterDetailsService() throws Exception {
		ResponseStatus<List<ProjectCharter>> responseStatus = new ResponseStatus<List<ProjectCharter>>();
		List<ProjectCharter> projectCharterDetails = null;

		try {
			projectCharterDetails = this.projectCharterRepository.GetProjectCharterDetailsRepo();
			responseStatus.setBody(projectCharterDetails);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method GetProjectCharterDetailsService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(projectCharterDetails);
			throw new Exception("GetProjectCharterDetailsService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<List<ProjectCharter>> GetProjectCharterByProjectIdService(int projectId) throws Exception {
		ResponseStatus<List<ProjectCharter>> responseStatus = new ResponseStatus<List<ProjectCharter>>();
		List<ProjectCharter> projectCharterDetails = null;

		try {
			projectCharterDetails = this.projectCharterRepository.GetProjectCharterByProjectIdRepo(projectId);
			responseStatus.setBody(projectCharterDetails);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method GetProjectCharterByProjectIdService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(projectCharterDetails);
			throw new Exception("GetProjectCharterByProjectIdService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<List<ProjectCharterResultArea>> GetProjectCharterResultAreaService(int p_ProjectId,
			int p_ProjectTypeId) throws Exception {
		ResponseStatus<List<ProjectCharterResultArea>> responseStatus = new ResponseStatus<List<ProjectCharterResultArea>>();
		List<ProjectCharterResultArea> projectCharterResultAreaDetails = null;
		try {
			projectCharterResultAreaDetails = this.projectCharterRepository.GetProjectCharterResultAreaRepo(p_ProjectId,
					p_ProjectTypeId);
			responseStatus.setBody(projectCharterResultAreaDetails);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method GetProjectCharterResultAreaService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(projectCharterResultAreaDetails);
			throw new Exception("GetProjectCharterResultAreaService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<List<ProjectCharterBenefits>> GetProjectCharterBenefitsService(int p_ProjectId,
			int p_ProjectTypeId) throws Exception {
		ResponseStatus<List<ProjectCharterBenefits>> responseStatus = new ResponseStatus<List<ProjectCharterBenefits>>();
		List<ProjectCharterBenefits> projectCharterBenefitsDetails = null;

		try {
			projectCharterBenefitsDetails = this.projectCharterRepository.GetProjectCharterBenefitsRepo(p_ProjectId,
					p_ProjectTypeId);
			responseStatus.setBody(projectCharterBenefitsDetails);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method GetProjectCharterBenefitsService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(projectCharterBenefitsDetails);
			throw new Exception("GetProjectCharterBenefitsService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<List<ProjectCharterSteeringComtProjectTeam>> GetProjectCharterSteeringComtProjectTeamService(
			int p_ProjectId, int p_ProjectTypeId) throws Exception {
		ResponseStatus<List<ProjectCharterSteeringComtProjectTeam>> responseStatus = new ResponseStatus<List<ProjectCharterSteeringComtProjectTeam>>();
		List<ProjectCharterSteeringComtProjectTeam> projectCharterBenefitsDetails = null;

		try {
			projectCharterBenefitsDetails = this.projectCharterRepository
					.GetProjectCharterSteeringComtProjectTeamRepo(p_ProjectId, p_ProjectTypeId);
			responseStatus.setBody(projectCharterBenefitsDetails);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method GetProjectCharterSteeringComtProjectTeamService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(projectCharterBenefitsDetails);
			throw new Exception("GetProjectCharterSteeringComtProjectTeamService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<List<ProjectCharterDmaicphaseToll>> GetProjectCharterDMAICPhaseService(int p_ProjectId,
			int p_ProjectTypeId) throws Exception {
		ResponseStatus<List<ProjectCharterDmaicphaseToll>> responseStatus = new ResponseStatus<List<ProjectCharterDmaicphaseToll>>();
		List<ProjectCharterDmaicphaseToll> projectCharterDmaicphaseTollDetails = null;

		try {
			projectCharterDmaicphaseTollDetails = this.projectCharterRepository
					.GetProjectCharterDMAICPhaseRepo(p_ProjectId, p_ProjectTypeId);
			responseStatus.setBody(projectCharterDmaicphaseTollDetails);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method GetProjectCharterDMAICPhaseService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(projectCharterDmaicphaseTollDetails);
			throw new Exception("GetProjectCharterDMAICPhaseService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<List<ProjectCharterSteeringCommitee>> GetProjectCharterSteeringCommitteService(
			int p_ProjectId, int p_ProjectTypeId) throws Exception {
		ResponseStatus<List<ProjectCharterSteeringCommitee>> responseStatus = new ResponseStatus<List<ProjectCharterSteeringCommitee>>();
		List<ProjectCharterSteeringCommitee> projectCharterSteeringCommiteeDetails = null;
		try {
			projectCharterSteeringCommiteeDetails = this.projectCharterRepository
					.GetProjectCharterSteeringCommitteRepo(p_ProjectId, p_ProjectTypeId);
			responseStatus.setBody(projectCharterSteeringCommiteeDetails);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method GetProjectCharterSteeringCommitteService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(projectCharterSteeringCommiteeDetails);
			throw new Exception("GetProjectCharterSteeringCommitteService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<List<ProjectCharterKeyStakeHolders>> GetProjectCharterKeyStakeHoldersService(int p_ProjectId,
			int p_ProjectTypeId) throws Exception {
		ResponseStatus<List<ProjectCharterKeyStakeHolders>> responseStatus = new ResponseStatus<List<ProjectCharterKeyStakeHolders>>();
		List<ProjectCharterKeyStakeHolders> projectCharterKeyStakeHolders = null;

		try {
			projectCharterKeyStakeHolders = this.projectCharterRepository
					.GetProjectCharterKeyStakeHoldersRepo(p_ProjectId, p_ProjectTypeId);
			responseStatus.setBody(projectCharterKeyStakeHolders);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method GetProjectCharterKeyStakeHoldersService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(projectCharterKeyStakeHolders);
			throw new Exception("GetProjectCharterKeyStakeHoldersService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<List<ProjectCharterDocuments>> GetProjectCharterDocumentsService(int p_ProjectId,
			int p_ProjectTypeId) throws Exception {
		ResponseStatus<List<ProjectCharterDocuments>> responseStatus = new ResponseStatus<List<ProjectCharterDocuments>>();
		List<ProjectCharterDocuments> projectCharterDocumentsDetails = null;

		try {
			projectCharterDocumentsDetails = this.projectCharterRepository.GetProjectCharterDocumentsRepo(p_ProjectId,
					p_ProjectTypeId);
			responseStatus.setBody(projectCharterDocumentsDetails);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method GetProjectCharterDocumentsService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(projectCharterDocumentsDetails);
			throw new Exception("GetProjectCharterDocumentsService() failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<List<ProjectCharterRevisionHistory>> GetProjectCharterRivisionHistoryService(int p_ProjectId,
			int p_ProjectTypeId) throws Exception {
		ResponseStatus<List<ProjectCharterRevisionHistory>> responseStatus = new ResponseStatus<List<ProjectCharterRevisionHistory>>();
		List<ProjectCharterRevisionHistory> projectCharterRevisionHistoryDetails = null;

		try {
			projectCharterRevisionHistoryDetails = this.projectCharterRepository
					.GetProjectCharterRivisionHistoryRepo(p_ProjectId, p_ProjectTypeId);
			responseStatus.setBody(projectCharterRevisionHistoryDetails);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method GetProjectCharterRivisionHistoryService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(projectCharterRevisionHistoryDetails);
			throw new Exception("GetProjectCharterRivisionHistoryService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<String> InsertProjectCharterResultAreaDetailService(int projectId, int projectTypeId,
			List<ProjectCharterResultArea> projectCharterResultArea) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {

			for (ProjectCharterResultArea projectCharterResultAreaEntity : projectCharterResultArea) {
				int insertResult = this.projectCharterRepository.InsertProjectCharterResultAreaDetailRepo(projectId,
						projectTypeId, projectCharterResultAreaEntity);
				if (insertResult >= 0) {
					log.info("Project Charter ResultArea Details Saved Successfully.");
					responseStatus.setBody("Project Charter ResultArea Details Saved Successfully.");
					responseStatus.setStatus(true);
					responseStatus.setDescription("Success");
				} else {
					log.error("error occured while saving Project Charter Result Area Details.");
					responseStatus.setBody("error occured while saving Project Charter Result Area Details.");
					responseStatus.setStatus(false);
					responseStatus.setDescription("Failed");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method InsertProjectCharterResultAreaDetailService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(
					"error occured while saving Project Charter Details in method InsertProjectCharterResultAreaDetailService().");
			throw new Exception("InsertProjectCharterResultAreaDetailService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<String> InsertProjectCharterdetailService(ProjectCharter projectCharter) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			int insertupdateProjectCharterResult = this.projectCharterRepository
					.InsertProjectCharterdetailRepo(projectCharter);

			if (insertupdateProjectCharterResult >= 0) {
				log.info("Project Charter Details Saved Successfully.");

				List<IdeaSubmission> ideaSubmissionDetails = this.ideaSubmissionRepository
						.GetIdeaSubmissionbyIdRepo(projectCharter.getIdeasubmissionId());

				this.serviceHelper.SendEmailUserToProjectCharter(projectCharter, insertupdateProjectCharterResult,
						ideaSubmissionDetails.get(0));

				SendEmailBTForProjectCharter(projectCharter, insertupdateProjectCharterResult,
						ideaSubmissionDetails.get(0));

				responseStatus.setBody("ProjectCharter = " + insertupdateProjectCharterResult);
				responseStatus.setStatus(true);
				responseStatus.setDescription("Success");
			} else {
				log.error("error occured while saving Project Charter Details.");
				responseStatus.setBody("error occured while saving Project Charter Details.");
				responseStatus.setStatus(false);
				responseStatus.setDescription("Failed");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method InsertProjectCharterdetailService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(
					"error occured while saving Project Charter Details in method InsertProjectCharterdetailService().");
			throw new Exception("InsertupdateProjectCharterdetailService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<String> UpdateProjectCharterdetailService(ProjectCharter projectCharter) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			this.projectCharterRepository.DeletePreviousProjectCharterDetailRepo(projectCharter.getId(),
					projectCharter.getProjecttypeId());
			int insertupdateProjectCharterResult = this.projectCharterRepository
					.UpdateProjectCharterdetailRepo(projectCharter);

			if (insertupdateProjectCharterResult >= 0) {
				log.info("Project Charter Details Update Successfully.");
				responseStatus.setBody("Project Charter Details Update Successfully.");
				responseStatus.setStatus(true);
				responseStatus.setDescription("Success");
			} else {
				log.error("error occured while updating Project Charter Details.");
				responseStatus.setBody("error occured while updating Project Charter Details.");
				responseStatus.setStatus(false);
				responseStatus.setDescription("Failed");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method UpdateProjectCharterdetailService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(
					"error occured while saving Project Charter Details in method UpdateProjectCharterdetailService().");
			throw new Exception("InsertupdateProjectCharterdetailService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<String> InsertProjectCharterDmaicphaseTollService(int projectId, int projectTypeId,
			List<ProjectCharterDmaicphaseToll> projectCharterDmaicphaseToll) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {

			for (ProjectCharterDmaicphaseToll projectCharterDmaicphaseTollEntity : projectCharterDmaicphaseToll) {
				int insertResult = this.projectCharterRepository.InsertProjectCharterDmaicphaseTollRepo(projectId,
						projectTypeId, projectCharterDmaicphaseTollEntity);

				if (insertResult >= 0) {
					log.info("Project Charter Details Saved Successfully.");
					responseStatus.setBody("Project Charter DmaicphaseToll Details Saved Successfully.");
					responseStatus.setStatus(true);
					responseStatus.setDescription("Success");
				} else {
					log.error("error occured while saving Project Charter DmaicphaseToll Details.");
					responseStatus.setBody("error occured while saving Project Charter DmaicphaseToll Details.");
					responseStatus.setStatus(false);
					responseStatus.setDescription("Failed");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method InsertProjectCharterDmaicphaseTollService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(
					"error occured while saving Project Charter Details in method InsertProjectCharterDmaicphaseTollService().");
			throw new Exception("InsertProjectCharterDmaicphaseTollService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<String> InsertProjectCharterSteeringCommitteDetailService(int projectId, int projectTypeId,
			ProjectCharterSteeringCommitee projectCharterSteeringCommitee) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			int insertResult = this.projectCharterRepository.InsertProjectCharterSteeringCommitteRepo(projectId,
					projectTypeId, projectCharterSteeringCommitee);

			if (insertResult >= 0) {
				log.info("Project Charter SteeringCommitte Details Saved Successfully.");
				responseStatus.setBody("ProjectCharter SteeringCommitte Id = " + insertResult);
				responseStatus.setStatus(true);
				responseStatus.setDescription("Success");
			} else {
				log.error("error occured while saving Project Charter SteeringCommitte Details.");
				responseStatus.setBody("error occured while saving Project Charter SteeringCommitte Details.");
				responseStatus.setStatus(false);
				responseStatus.setDescription("Failed");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method InsertProjectCharterSteeringCommitteDetailService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(
					"error occured while saving Project Charter Details in method InsertProjectCharterSteeringCommitteDetailService().");
			throw new Exception("InsertProjectCharterSteeringCommitteDetailService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<String> InsertProjectCharterSteeringCommitteTeamDetailService(int projectId,
			int projectTypeId, List<ProjectCharterSteeringComtProjectTeam> projectCharterSteeringComtProjectTeam)
			throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {

			for (ProjectCharterSteeringComtProjectTeam projectCharterSteeringComtProjectTeamEntity : projectCharterSteeringComtProjectTeam) {

				int insertResult = this.projectCharterRepository.InsertProjectCharterSteeringCommitteTeamRepo(projectId,
						projectTypeId, projectCharterSteeringComtProjectTeamEntity);

				if (insertResult >= 0) {
					log.info("Project Charter Details Saved Successfully.");
					responseStatus.setBody("Project Charter SteeringCommitte Team Details Saved Successfully.");
					responseStatus.setStatus(true);
					responseStatus.setDescription("Success");
				} else {
					log.error("error occured while saving Project Charter SteeringCommitte Team Details.");
					responseStatus.setBody("error occured while saving Project Charter SteeringCommitte Team Details.");
					responseStatus.setStatus(false);
					responseStatus.setDescription("Failed");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method InsertProjectCharterSteeringCommitteTeamDetailService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(
					"error occured while saving Project Charter Details in method InsertProjectCharterSteeringCommitteTeamDetailService().");
			throw new Exception("InsertProjectCharterSteeringCommitteTeamDetailService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<String> InsertProjectCharterBenefitDetailService(int projectId, int projectTypeId,
			List<ProjectCharterBenefits> projectCharterBenefits) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			for (ProjectCharterBenefits projectCharterBenefitsEntity : projectCharterBenefits) {
				int insertResult = this.projectCharterRepository.InsertProjectCharterBenefitDetailRepo(projectId,
						projectTypeId, projectCharterBenefitsEntity);

				if (insertResult >= 0) {
					log.info("Project Charter Benefit Details Saved Successfully.");
					responseStatus.setBody("Project Charter Benefit Detail Saved Successfully.");
					responseStatus.setStatus(true);
					responseStatus.setDescription("Success");
				} else {
					log.error("error occured while saving Project Charter BenefitDetail.");
					responseStatus.setBody("error occured while saving Project Charter Benefit Detail.");
					responseStatus.setStatus(false);
					responseStatus.setDescription("Failed");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method InsertProjectCharterBenefitDetailService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(
					"error occured while saving Project Charter Details in method InsertProjectCharterBenefitDetailService().");
			throw new Exception("InsertProjectCharterBenefitDetailService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<String> InsertProjectCharterKeyStakeHolderdetailService(int projectId, int projectTypeId,
			List<ProjectCharterKeyStakeHolders> projectCharterKeyStakeHolders) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			for (ProjectCharterKeyStakeHolders ProjectCharterKeyStakeHoldersEntity : projectCharterKeyStakeHolders) {
				int insertResult = this.projectCharterRepository.InsertProjectCharterKeyStakeHolderdetailRepo(projectId,
						projectTypeId, ProjectCharterKeyStakeHoldersEntity);

				if (insertResult >= 0) {
					log.info("Project Charter KeyStakeHolderdetail Details Saved Successfully.");
					responseStatus.setBody("Project Charter KeyStakeHolder Details Saved Successfully.");
					responseStatus.setStatus(true);
					responseStatus.setDescription("Success");
				} else {
					log.error("error occured while saving Project Charter KeyStakeHolder Details.");
					responseStatus.setBody("error occured while saving Project Charter KeyStakeHolder Details.");
					responseStatus.setStatus(false);
					responseStatus.setDescription("Failed");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method InsertProjectCharterKeyStakeHolderdetailService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(
					"error occured while saving Project Charter Details in method InsertProjectCharterKeyStakeHolderdetailService().");
			throw new Exception("InsertProjectCharterKeyStakeHolderdetailService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<String> InsertProjectCharterDocumentDetailService(int projectId, int projectTypeId,
			List<ProjectCharterDocuments> projectCharterDocuments) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			for (ProjectCharterDocuments projectCharterDocumentsEntity : projectCharterDocuments) {
				int insertResult = this.projectCharterRepository.InsertProjectCharterDocumentDetailRepo(projectId,
						projectTypeId, projectCharterDocumentsEntity);

				if (insertResult >= 0) {
					log.info("Project Charter Details Saved Successfully.");
					responseStatus.setBody("Project Charter DocumentDetail Saved Successfully.");
					responseStatus.setStatus(true);
					responseStatus.setDescription("Success");
				} else {
					log.error("error occured while saving Project Charter DocumentDetail.");
					responseStatus.setBody("error occured while saving Project Charter DocumentDetail Details.");
					responseStatus.setStatus(false);
					responseStatus.setDescription("Failed");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method InsertProjectCharterDocumentDetailService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(
					"error occured while saving Project Charter Details in method InsertProjectCharterDocumentDetailService().");
			throw new Exception("InsertProjectCharterDocumentDetailService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<String> InsertProjectCharterRevisionHistoryService(int projectId, int projectTypeId,
			List<ProjectCharterRevisionHistory> projectCharterRevisionHistory) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();

		try {

			for (ProjectCharterRevisionHistory projectCharterRevisionHistoryEntity : projectCharterRevisionHistory) {

				int insertResult = this.projectCharterRepository.InsertProjectCharterRevisionHistoryRepo(projectId,
						projectTypeId, projectCharterRevisionHistoryEntity);

				if (insertResult >= 0) {
					log.info("Project Charter RevisionHistory Details Saved Successfully.");
					responseStatus.setBody("Project Charter RevisionHistory Details Saved Successfully.");
					responseStatus.setStatus(true);
					responseStatus.setDescription("Success");
				} else {
					log.error("error occured while saving Project Charter RevisionHistory Details.");
					responseStatus.setBody("error occured while saving Project Charter RevisionHistory Details.");
					responseStatus.setStatus(false);
					responseStatus.setDescription("Failed");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method InsertProjectCharterRevisionHistoryService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(
					"error occured while saving Project Charter Details in method InsertProjectCharterRevisionHistoryService().");
			throw new Exception("InsertProjectCharterRevisionHistoryService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<String> UpdateProjectCharterStatusService(int projectId, String updatedBy, String comments,
			int projectStatusId) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();

		try {

			int updateStatusResult = this.projectCharterRepository.UpdateProjectCharterStatusRepo(projectId, updatedBy,
					comments, projectStatusId);
			if (updateStatusResult >= 0) {
				log.info("Project Charter status updated Successfully.");

				List<ProjectCharter> projectCharterDetails = this.projectCharterRepository
						.GetProjectCharterByProjectIdRepo(projectId);

				List<IdeaSubmission> ideaSubmissionDetails = this.ideaSubmissionRepository
						.GetIdeaSubmissionbyIdRepo(projectCharterDetails.get(0).getIdeasubmissionId());

				if (projectStatusId == 1) {
					SendEmailBTForProjectCharterApproval(projectCharterDetails.get(0), projectId,
							ideaSubmissionDetails.get(0));
				} else if (projectStatusId == 2) {
					SendEmailBTForProjectCharterRejected(projectCharterDetails.get(0), projectId,
							ideaSubmissionDetails.get(0));
				} else if (projectStatusId == 5) {
					SendEmailBTForProjectCharterReturn(projectCharterDetails.get(0), projectId,
							ideaSubmissionDetails.get(0));
				}
				responseStatus.setBody("Project Charter status updated Successfully.");
				responseStatus.setStatus(true);
				responseStatus.setDescription("Success");
			} else {
				log.error("error occured while updating Project Charter status Details.");
				responseStatus.setBody("error occured while updating Project Charter status Details.");
				responseStatus.setStatus(false);
				responseStatus.setDescription("Failed");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method UpdateProjectCharterStatusService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(
					"error occured while saving Project Charter Details in method UpdateProjectCharterStatusService().");
			throw new Exception("UpdateProjectCharterStatusService failed.");
		}
		return responseStatus;
	}

	private int SendEmailBTForProjectCharter(ProjectCharter projectCharter, int insertProjectCharterResult,
			IdeaSubmission ideaSubmissionDetails) throws Exception {

		int result = 0;

		try {
			int emailsentResult = this.serviceHelper.SendEmailBTForProjectCharter(projectCharter,
					insertProjectCharterResult, ideaSubmissionDetails);
			if (emailsentResult >= 0) {
				log.info("Email Sent.");
			}
			result = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method SendEmailBTForProjectCharter(): "
							+ e.getMessage());
			result = -1;
			throw new Exception("SendEmailBTForProjectCharter failed.");
		}
		return result;
	}

	private int SendEmailBTForProjectCharterApproval(ProjectCharter projectCharter, int insertProjectCharterResult,
			IdeaSubmission ideaSubmissionDetails) throws Exception {

		int result = 0;

		try {
			int emailsentResult = this.serviceHelper.SendEmailBTForProjectCharter(projectCharter,
					insertProjectCharterResult, ideaSubmissionDetails);

			int emailsentUserApprovalResult = this.serviceHelper.SendEmailAfterBTApproveProject(projectCharter,
					insertProjectCharterResult, ideaSubmissionDetails);

			if (emailsentResult >= 0) {
				log.info("Email Sent.");
			}
			result = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method SendEmailBTForProjectCharterApproval(): "
							+ e.getMessage());
			result = -1;
			throw new Exception("SendEmailBTForProjectCharterApproval failed.");
		}
		return result;
	}

	private int SendEmailBTForProjectCharterRejected(ProjectCharter projectCharter, int insertProjectCharterResult,
			IdeaSubmission ideaSubmissionDetails) throws Exception {

		int result = 0;

		try {
			int emailsentResult = this.serviceHelper.SendEmailUserForRejectProject(projectCharter,
					insertProjectCharterResult, ideaSubmissionDetails);

			int emailsentUserRejectResult = this.serviceHelper.SendEmailUserForRejectProject(projectCharter,
					insertProjectCharterResult, ideaSubmissionDetails);

			if (emailsentResult >= 0) {
				log.info("Email Sent.");
			}
			result = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method SendEmailBTForProjectCharterRejected(): "
							+ e.getMessage());
			result = -1;
			throw new Exception("SendEmailBTForProjectCharterRejected failed.");
		}
		return result;
	}

	private int SendEmailBTForProjectCharterReturn(ProjectCharter projectCharter, int insertProjectCharterResult,
			IdeaSubmission ideaSubmissionDetails) throws Exception {

		int result = 0;

		try {
			int emailsentResult = this.serviceHelper.SendEmailUserToProjectCharterReturn(projectCharter,
					insertProjectCharterResult, ideaSubmissionDetails);

			int emailsentUserReturnResult = this.serviceHelper.SendEmailUserToProjectCharterReturn(projectCharter,
					insertProjectCharterResult, ideaSubmissionDetails);

			if (emailsentResult >= 0) {
				log.info("Email Sent.");
			}
			result = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterServiceImpl method SendEmailBTForProjectCharterReturn(): "
							+ e.getMessage());
			result = -1;
			throw new Exception("SendEmailBTForProjectCharterReturn failed.");
		}
		return result;
	}

}
