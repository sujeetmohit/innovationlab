package com.ais.innovationLab.api.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ais.innovationLab.api.entity.DMAIC_Phase;
import com.ais.innovationLab.api.entity.LeverTechnology;
import com.ais.innovationLab.api.entity.ProjectStatus;
import com.ais.innovationLab.api.entity.ProjectType;
import com.ais.innovationLab.api.entity.ResponseStatus;
import com.ais.innovationLab.api.entity.ResultArea;
import com.ais.innovationLab.api.repository.CommonRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CommonServiceImpl implements CommonService {

	@Autowired
	private CommonRepository commonRepository;

	public ResponseStatus<List<ProjectType>> GetProjectTypeDetailService() throws Exception {
		ResponseStatus<List<ProjectType>> responseStatus = new ResponseStatus<List<ProjectType>>();
		List<ProjectType> projectTypeDetails = null;

		try {
			projectTypeDetails = this.commonRepository.GetProjectTypeRepo();
			responseStatus.setBody(projectTypeDetails);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in CommonServiceImpl method GetProjectTypeDetailService(): "
					+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(projectTypeDetails);
			throw new Exception("GetProjectTypeDetailService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<List<ProjectStatus>> GetProjectStatusService() throws Exception {
		ResponseStatus<List<ProjectStatus>> responseStatus = new ResponseStatus<List<ProjectStatus>>();
		List<ProjectStatus> projectStatusDetails = null;

		try {
			projectStatusDetails = this.commonRepository.GetProjectStatusRepo();
			responseStatus.setBody(projectStatusDetails);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in CommonServiceImpl method GetProjectStatusService(): "
					+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(projectStatusDetails);
			throw new Exception("GetProjectStatusService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<List<LeverTechnology>> GetLeverTechnologyDetailService() throws Exception {
		ResponseStatus<List<LeverTechnology>> responseStatus = new ResponseStatus<List<LeverTechnology>>();
		List<LeverTechnology> leverTechnology = null;

		try {
			leverTechnology = this.commonRepository.GetLeverTechnologyRepo();
			responseStatus.setBody(leverTechnology);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in CommonServiceImpl method GetLeverTechnologyDetailService(): "
					+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(leverTechnology);
			throw new Exception("GetLeverTechnologyDetailService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<List<DMAIC_Phase>> GetDMAIC_PhaseDetailService() throws Exception {
		ResponseStatus<List<DMAIC_Phase>> responseStatus = new ResponseStatus<List<DMAIC_Phase>>();
		List<DMAIC_Phase> dmaic_Phases = null;
		try {
			dmaic_Phases = this.commonRepository.GetDMAIC_PhaseRepo();
			responseStatus.setBody(dmaic_Phases);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in CommonServiceImpl method GetDMAIC_PhaseDetailService():" + e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(dmaic_Phases);
			throw new Exception("GetDMAIC_PhaseDetailService failed.");
			
		}
		return responseStatus;
	}

	@Override
	public ResponseStatus<List<ResultArea>> GetResultAreaDetailService() throws Exception {
		ResponseStatus<List<ResultArea>> responseStatus = new ResponseStatus<List<ResultArea>>();
		List<ResultArea> resultarea = null;
		try {
			resultarea = this.commonRepository.GetResultAreaRepo();
			responseStatus.setBody(resultarea);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in CommonServiceImpl method GetResultAreaDetailService():" + e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(resultarea);
			throw new Exception("GetResultAreaDetailService failed.");
			
		}
		return responseStatus;
	}

}
