package com.ais.innovationLab.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ais.innovationLab.api.entity.Report;
import com.ais.innovationLab.api.entity.ResponseStatus;
import com.ais.innovationLab.api.repository.ReportRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ReportServiceImpl implements ReportService {

	@Autowired
	private ReportRepository reportRepository;

	public ResponseStatus<Report> GetDashBoardReportService() throws Exception {
		ResponseStatus<Report> responseStatus = new ResponseStatus<Report>();
		Report dashBoardReport = new Report();
		try {

//			1 for idea report
			dashBoardReport.setReportIdea(this.reportRepository.GetIdeaReportRepo(1));

//			2 for project charter report
			dashBoardReport.setReportProject(this.reportRepository.GetProjectCharterReportRepo(2));

			responseStatus.setBody(dashBoardReport);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in ReportServiceImpl method GetDashBoardReportService(): "
					+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(dashBoardReport);
			throw new Exception("GetDashBoardReportService failed.");
		}
		return responseStatus;
	}

}
