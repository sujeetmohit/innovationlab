package com.ais.innovationLab.api.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ais.innovationLab.api.entity.IdeaSubmission;
import com.ais.innovationLab.api.entity.ResponseStatus;
import com.ais.innovationLab.api.helper.EmailServiceHelper;
import com.ais.innovationLab.api.repository.IdeaSubmissionRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IdeaSubmissionServiceImpl implements IdeaSubmissionService {

	@Autowired
	private IdeaSubmissionRepository ideaSubmissionRepository;

	@Autowired
	private EmailServiceHelper serviceHelper;

	public ResponseStatus<List<IdeaSubmission>> GetIdeaSubmissionDetailsService(String loginEmailId) throws Exception {
		ResponseStatus<List<IdeaSubmission>> responseStatus = new ResponseStatus<List<IdeaSubmission>>();
		List<IdeaSubmission> ideaSubmissionDetails = null;

		try {
			ideaSubmissionDetails = this.ideaSubmissionRepository.GetIdeaSubmissionDetailsRepo(loginEmailId);
			responseStatus.setBody(ideaSubmissionDetails);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in IdeaSubmissionServiceImpl method GetIdeaSubmissionDetailsService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(ideaSubmissionDetails);
			throw new Exception("GetIdeaSubmissionDetailsService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<List<IdeaSubmission>> GetIdeaSubmissionbyIdService(int ideaSubmissionId) throws Exception {
		ResponseStatus<List<IdeaSubmission>> responseStatus = new ResponseStatus<List<IdeaSubmission>>();
		List<IdeaSubmission> ideaSubmissionDetails = null;

		try {
			ideaSubmissionDetails = this.ideaSubmissionRepository.GetIdeaSubmissionbyIdRepo(ideaSubmissionId);

			responseStatus.setBody(ideaSubmissionDetails);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in IdeaSubmissionServiceImpl method GetIdeaSubmissionbyIdService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(ideaSubmissionDetails);
			throw new Exception("GetIdeaSubmissionbyIdService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<String> InsertIdeaSubmissiondetailService(IdeaSubmission ideaSubmission) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {

			int insertIdeaSubmissionResult = this.ideaSubmissionRepository
					.InsertIdeaSubmissionDetailRepo(ideaSubmission);

			if (insertIdeaSubmissionResult >= 0) {
				log.info("Idea Submission Details Saved Successfully.");

				this.serviceHelper.SendEmailUserForIdeaSubmission(ideaSubmission, insertIdeaSubmissionResult);
				SendEmailBTForIdeaSubmission(ideaSubmission, insertIdeaSubmissionResult);
				responseStatus.setBody("IdeaSubmission = " + insertIdeaSubmissionResult);
				responseStatus.setStatus(true);
				responseStatus.setDescription("Success");
			} else {
				log.error("error occured while saving Idea Submission Details.");
				responseStatus.setBody("error occured while saving Idea Submission Details.");
				responseStatus.setStatus(false);
				responseStatus.setDescription("Failed");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in IdeaSubmissionServiceImpl method InsertIdeaSubmissiondetailService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(
					"error occured while saving Idea Submission Details in method InsertIdeaSubmissiondetailService().");
			throw new Exception("InsertIdeaSubmissiondetailService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<String> UpdateManagerStatusIdeaSubmissiondetailService(int ideaSubmissionId,
			int managerStatusId, String managerFeedback) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {

			int updateIdeaSubmissionResult = this.ideaSubmissionRepository
					.UpdateManagerStatusIdeaSubmissiondetailRepo(ideaSubmissionId, managerStatusId, managerFeedback);
			IdeaSubmission ideaSubmission = this.ideaSubmissionRepository.GetIdeaSubmissionbyIdRepo(ideaSubmissionId)
					.get(0);
			if (updateIdeaSubmissionResult >= 0) {

				log.info("Idea Submission Details Saved Successfully.");

				if (managerStatusId == 1) {
					this.serviceHelper.SendEmailAfterBTApproveIdea(ideaSubmission, ideaSubmissionId);
				} else if (managerStatusId == 2) {
					SendEmailBTForIdeaSubmissionReject(ideaSubmission, ideaSubmissionId);
				} else if (managerStatusId == 5) {
					SendEmailBTForIdeaSubmissionReturn(ideaSubmission, ideaSubmissionId);
				}
				responseStatus.setBody("Idea Submission Details Saved Successfully.");
				responseStatus.setStatus(true);
				responseStatus.setDescription("Success");
			} else {
				log.error("error occured while saving Idea Submission Details.");
				responseStatus.setBody("error occured while saving Idea Submission Details.");
				responseStatus.setStatus(false);
				responseStatus.setDescription("Failed");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in IdeaSubmissionServiceImpl method UpdateManagerStatusIdeaSubmissiondetailService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(
					"error occured while saving Idea Submission Details in method UpdateManagerStatusIdeaSubmissiondetailService().");
			throw new Exception("UpdateManagerStatusIdeaSubmissiondetailService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<List<IdeaSubmission>> GetManagerStatusIdeaSubmissionDetailsService(String loginEmailId,
			int managerStatusId) throws Exception {
		ResponseStatus<List<IdeaSubmission>> responseStatus = new ResponseStatus<List<IdeaSubmission>>();
		List<IdeaSubmission> ideaSubmissionDetails = null;

		try {
			ideaSubmissionDetails = this.ideaSubmissionRepository
					.GetManagerStatusIdeaSubmissionDetailsRepo(loginEmailId, managerStatusId);
			responseStatus.setBody(ideaSubmissionDetails);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in IdeaSubmissionServiceImpl method GetIdeaSubmissionDetailsService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(ideaSubmissionDetails);
			throw new Exception("GetIdeaSubmissionDetailsService failed.");
		}
		return responseStatus;
	}

	public ResponseStatus<String> UpdateIdeaSubmissiondetailService(IdeaSubmission ideaSubmission) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {

			int insertIdeaSubmissionResult = this.ideaSubmissionRepository
					.UpdateIdeaSubmissionDetailRepo(ideaSubmission);

			if (insertIdeaSubmissionResult >= 0) {
				log.info("Idea Submission Details Update Successfully.");
				responseStatus.setBody("IdeaSubmission = " + insertIdeaSubmissionResult);
				responseStatus.setStatus(true);
				responseStatus.setDescription("Success");
			} else {
				log.error("error occured while updating Idea Submission Details.");
				responseStatus.setBody("error occured while updating Idea Submission Details.");
				responseStatus.setStatus(false);
				responseStatus.setDescription("Failed");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in IdeaSubmissionServiceImpl method UpdateIdeaSubmissiondetailService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(
					"error occured while saving Idea Submission Details in method UpdateIdeaSubmissiondetailService().");
			throw new Exception("UpdateIdeaSubmissiondetailService failed.");
		}
		return responseStatus;
	}

	private int SendEmailBTForIdeaSubmission(IdeaSubmission ideaSubmission, int insertIdeaSubmissionResult)
			throws Exception {

		int result = 0;

		try {
			int emailsentResult = this.serviceHelper.SendEmailBTForIdeaSubmission(ideaSubmission,
					insertIdeaSubmissionResult);
			if (emailsentResult >= 0) {
				log.info("Email Sent.");
			}
			result = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in EmployeeServiceImpl method SendEmailBTForIdeaSubmission(): "
					+ e.getMessage());
			result = -1;
			throw new Exception("SendEmailBTForIdeaSubmission failed.");
		}
		return result;
	}

	private int SendEmailBTForIdeaSubmissionReject(IdeaSubmission ideaSubmission, int insertIdeaSubmissionResult)
			throws Exception {

		int result = 0;

		try {
			int emailsentResult = this.serviceHelper.SendEmailUserForRejectIdea(ideaSubmission,
					insertIdeaSubmissionResult);
			if (emailsentResult >= 0) {
				log.info("Email Sent.");
			}
			result = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in EmployeeServiceImpl method SendEmailBTForIdeaSubmissionReject(): "
							+ e.getMessage());
			result = -1;
			throw new Exception("SendEmailBTForIdeaSubmissionReject failed.");
		}
		return result;
	}

	private int SendEmailBTForIdeaSubmissionReturn(IdeaSubmission ideaSubmission, int insertIdeaSubmissionResult)
			throws Exception {

		int result = 0;

		try {
			int emailsentResult = this.serviceHelper.SendEmailUserForIdeaReturn(ideaSubmission,
					insertIdeaSubmissionResult);
			if (emailsentResult >= 0) {
				log.info("Email Sent.");
			}
			result = 1;
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in EmployeeServiceImpl method SendEmailBTForIdeaSubmissionReturn(): "
							+ e.getMessage());
			result = -1;
			throw new Exception("SendEmailBTForIdeaSubmissionReturn failed.");
		}
		return result;
	}

}
