package com.ais.innovationLab.api.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ais.innovationLab.api.entity.ResultType;
import com.ais.innovationLab.api.entity.ResponseStatus;

@Service
public interface ResultTypeSevice {

	public ResponseStatus<List<ResultType>> GetResultTypedetailService() throws Exception;

	//public ResponseStatus<String> InsertupdateResultTypedetailService() throws Exception;

}
