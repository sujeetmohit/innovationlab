package com.ais.innovationLab.api.service;

import java.util.List;
import org.springframework.stereotype.Service;

import com.ais.innovationLab.api.entity.DMAIC_Phase;
import com.ais.innovationLab.api.entity.LeverTechnology;
import com.ais.innovationLab.api.entity.ProjectStatus;
import com.ais.innovationLab.api.entity.ProjectType;
import com.ais.innovationLab.api.entity.ResponseStatus;
import com.ais.innovationLab.api.entity.ResultArea;

@Service
public interface CommonService {

	public ResponseStatus<List<ProjectType>> GetProjectTypeDetailService() throws Exception;

	public ResponseStatus<List<ProjectStatus>> GetProjectStatusService() throws Exception;

	public ResponseStatus<List<LeverTechnology>> GetLeverTechnologyDetailService() throws Exception;
	
	public ResponseStatus<List<DMAIC_Phase>> GetDMAIC_PhaseDetailService() throws Exception;
	
	public ResponseStatus<List<ResultArea>> GetResultAreaDetailService() throws Exception;


}
