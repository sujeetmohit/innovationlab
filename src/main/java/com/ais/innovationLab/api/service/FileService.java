package com.ais.innovationLab.api.service;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.ais.innovationLab.api.entity.FileModel;
import com.ais.innovationLab.api.entity.ResponseStatus;

@Service
public interface FileService {

	public ResponseStatus<Integer> DeleteIdeaSubmissionService(int ideaSubmissionId) throws Exception;

	public ResponseStatus<String> uplaodIdeaSubmissionService(FileModel file, int ideaSubmissionId) throws Exception;

	public Resource DownloadIdeaSubmissionService(int ideaSubmissionId) throws Exception;

}
