package com.ais.innovationLab.api.service;

import java.util.List;
import org.springframework.stereotype.Service;
import com.ais.innovationLab.api.entity.ProjectCharter;
import com.ais.innovationLab.api.entity.ProjectCharterBenefits;
import com.ais.innovationLab.api.entity.ProjectCharterDmaicphaseToll;
import com.ais.innovationLab.api.entity.ProjectCharterDocuments;
import com.ais.innovationLab.api.entity.ProjectCharterKeyStakeHolders;
import com.ais.innovationLab.api.entity.ProjectCharterResultArea;
import com.ais.innovationLab.api.entity.ProjectCharterRevisionHistory;
import com.ais.innovationLab.api.entity.ProjectCharterSteeringCommitee;
import com.ais.innovationLab.api.entity.ProjectCharterSteeringComtProjectTeam;
import com.ais.innovationLab.api.entity.ResponseStatus;

@Service
public interface ProjectCharterService {

	public ResponseStatus<List<ProjectCharter>> GetProjectCharterDetailsService() throws Exception;

	public ResponseStatus<List<ProjectCharter>> GetProjectCharterByProjectIdService(int projectId) throws Exception;

	public ResponseStatus<List<ProjectCharterResultArea>> GetProjectCharterResultAreaService(int p_ProjectId,
			int p_ProjectTypeId) throws Exception;

	public ResponseStatus<List<ProjectCharterBenefits>> GetProjectCharterBenefitsService(int p_ProjectId,
			int p_ProjectTypeId) throws Exception;

	public ResponseStatus<List<ProjectCharterSteeringComtProjectTeam>> GetProjectCharterSteeringComtProjectTeamService(
			int p_ProjectId, int p_ProjectTypeId) throws Exception;

	public ResponseStatus<List<ProjectCharterDmaicphaseToll>> GetProjectCharterDMAICPhaseService(int p_ProjectId,
			int p_ProjectTypeId) throws Exception;

	public ResponseStatus<List<ProjectCharterSteeringCommitee>> GetProjectCharterSteeringCommitteService(
			int p_ProjectId, int p_ProjectTypeId) throws Exception;

	public ResponseStatus<List<ProjectCharterKeyStakeHolders>> GetProjectCharterKeyStakeHoldersService(int p_ProjectId,
			int p_ProjectTypeId) throws Exception;

	public ResponseStatus<List<ProjectCharterDocuments>> GetProjectCharterDocumentsService(int p_ProjectId,
			int p_ProjectTypeId) throws Exception;

	public ResponseStatus<List<ProjectCharterRevisionHistory>> GetProjectCharterRivisionHistoryService(int p_ProjectId,
			int p_ProjectTypeId) throws Exception;

	public ResponseStatus<String> InsertProjectCharterdetailService(ProjectCharter projectCharter) throws Exception;

	public ResponseStatus<String> UpdateProjectCharterdetailService(ProjectCharter projectCharter) throws Exception;

	public ResponseStatus<String> InsertProjectCharterResultAreaDetailService(int projectId, int projectTypeId,
			List<ProjectCharterResultArea> projectCharterResultArea) throws Exception;

	public ResponseStatus<String> InsertProjectCharterDmaicphaseTollService(int projectId, int projectTypeId,
			List<ProjectCharterDmaicphaseToll> projectCharterDmaicphaseToll) throws Exception;

	public ResponseStatus<String> InsertProjectCharterSteeringCommitteDetailService(int projectId, int projectTypeId,
			ProjectCharterSteeringCommitee projectCharterSteeringCommitee) throws Exception;

	public ResponseStatus<String> InsertProjectCharterSteeringCommitteTeamDetailService(int projectId,
			int projectTypeId, List<ProjectCharterSteeringComtProjectTeam> projectCharterSteeringComtProjectTeam)
			throws Exception;

	public ResponseStatus<String> InsertProjectCharterBenefitDetailService(int projectId, int projectTypeId,
			List<ProjectCharterBenefits> projectCharterBenefits) throws Exception;

	public ResponseStatus<String> InsertProjectCharterKeyStakeHolderdetailService(int projectId, int projectTypeId,
			List<ProjectCharterKeyStakeHolders> projectCharterKeyStakeHolders) throws Exception;

	public ResponseStatus<String> InsertProjectCharterDocumentDetailService(int projectId, int projectTypeId,
			List<ProjectCharterDocuments> projectCharterDocuments) throws Exception;

	public ResponseStatus<String> InsertProjectCharterRevisionHistoryService(int projectId, int projectTypeId,
			List<ProjectCharterRevisionHistory> projectCharterRevisionHistory) throws Exception;

	public ResponseStatus<String> UpdateProjectCharterStatusService(int projectId, String updatedBy, String comments,
			int projectStatusId) throws Exception;

}
