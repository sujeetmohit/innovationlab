package com.ais.innovationLab.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.ais.innovationLab.api.entity.FileModel;
import com.ais.innovationLab.api.entity.ResponseStatus;
import com.ais.innovationLab.api.repository.FileRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FileServiceImpl implements FileService {

	@Autowired
	private FileRepository fileRepository;

	// @Value("${document.directory}")
	// private String documentRootDir;

	public ResponseStatus<Integer> DeleteIdeaSubmissionService(int ideaSubmissionId) throws Exception {
		ResponseStatus<Integer> responseStatus = new ResponseStatus<Integer>();
		int result = -1;
		try {
			log.info("requisition document deleted for IdeaSubmissionId = " + ideaSubmissionId);

			result = this.fileRepository.DeleteIdeaSubmissionRepo(ideaSubmissionId);
			this.fileRepository.UpdateReplaceIdeaSubmissionDocRepo(null, ideaSubmissionId);
			if (result > 0) {
				responseStatus.setBody(result);
				responseStatus.setStatus(true);
				responseStatus.setDescription("Success");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in FileServiceImpl method DeleteIdeaSubmissionService(): "
					+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(result);
			throw new Exception("DeleteIdeaSubmissionService failed.");
		}
		return responseStatus;
	}

	public Resource DownloadIdeaSubmissionService(int ideaSubmissionId) throws Exception {
		return this.fileRepository.DownloadIdeaSubmissionRepo(ideaSubmissionId);
	}

	public ResponseStatus<String> uplaodIdeaSubmissionService(FileModel file, int ideaSubmissionId) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		String result = "";
		try {
			if (file != null && file.getMultipartFile().getBytes().length > 0) {
				log.info("Original document Byte Size = " + file.getMultipartFile().getBytes().length);
				result = this.fileRepository.uplaodIdeaSubmissionRepo(file, ideaSubmissionId);
				this.fileRepository.UpdateReplaceIdeaSubmissionDocRepo(file.getName(), ideaSubmissionId);
				responseStatus.setBody(result);
				responseStatus.setStatus(true);
				responseStatus.setDescription("Success");
			} else {
				log.info("Original document Byte Size = 0");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in FileServiceImpl method uplaodIdeaSubmissionService(): "
					+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody("error occured while uploading IdeaSubmission document.");
			throw new Exception("uplaodIdeaSubmissionService failed.");
		}
		return responseStatus;

	}
}
