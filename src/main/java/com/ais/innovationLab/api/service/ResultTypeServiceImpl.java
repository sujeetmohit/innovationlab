package com.ais.innovationLab.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ais.innovationLab.api.entity.ResultType;
import com.ais.innovationLab.api.repository.ResultTypeRepository;
import com.ais.innovationLab.api.entity.ResponseStatus;

import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class ResultTypeServiceImpl implements ResultTypeSevice {

	@Autowired
	private ResultTypeRepository ResultTypeRepos;

	public ResponseStatus<List<ResultType>> GetResultTypedetailService() throws Exception {
		ResponseStatus<List<ResultType>> responseStatus = new ResponseStatus<List<ResultType>>();
		List<ResultType> ResultTypeDetails = null;

		try {
			ResultTypeDetails = this.ResultTypeRepos.GetResultTypeRepo();
			responseStatus.setBody(ResultTypeDetails);
			responseStatus.setStatus(true);
			responseStatus.setDescription("Success");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ResultTypeImpl method GetResultTypeService(): "
							+ e.getMessage());
			responseStatus.setStatus(false);
			responseStatus.setDescription(e.getMessage());
			responseStatus.setBody(ResultTypeDetails);
			throw new Exception("GetResultTypeService failed.");
		}
		return responseStatus;
	}

}
