package com.ais.innovationLab.api.service;

import java.util.List;
import org.springframework.stereotype.Service;
import com.ais.innovationLab.api.entity.IdeaSubmission;
import com.ais.innovationLab.api.entity.ResponseStatus;

@Service
public interface IdeaSubmissionService {

	public ResponseStatus<List<IdeaSubmission>> GetIdeaSubmissionDetailsService(String loginEmailId) throws Exception;

	public ResponseStatus<List<IdeaSubmission>> GetIdeaSubmissionbyIdService(int ideaSubmissionId) throws Exception;

	public ResponseStatus<String> InsertIdeaSubmissiondetailService(IdeaSubmission ideaSubmission) throws Exception;
	
	public ResponseStatus<String> UpdateIdeaSubmissiondetailService(IdeaSubmission ideaSubmission) throws Exception;

	public ResponseStatus<String> UpdateManagerStatusIdeaSubmissiondetailService(int ideaSubmissionId,
			int managerStatusId, String managerFeedback) throws Exception;

	public ResponseStatus<List<IdeaSubmission>> GetManagerStatusIdeaSubmissionDetailsService(String loginEmailId,
			int managerStatusId) throws Exception;
}
