package com.ais.innovationLab.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import lombok.extern.slf4j.Slf4j;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
@Slf4j
@EnableSwagger2
public class Application extends SpringBootServletInitializer {

	public static void main(String[] args) {
		log.info("INNOVATIONLAB service starting.");
		SpringApplication.run(Application.class, args);
	}
}
