package com.ais.innovationLab.api.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.ais.innovationLab.api.entity.ProjectCharter;
import com.ais.innovationLab.api.entity.ProjectCharterBenefits;
import com.ais.innovationLab.api.entity.ProjectCharterDmaicphaseToll;
import com.ais.innovationLab.api.entity.ProjectCharterDocuments;
import com.ais.innovationLab.api.entity.ProjectCharterKeyStakeHolders;
import com.ais.innovationLab.api.entity.ProjectCharterResultArea;
import com.ais.innovationLab.api.entity.ProjectCharterRevisionHistory;
import com.ais.innovationLab.api.entity.ProjectCharterSteeringCommitee;
import com.ais.innovationLab.api.entity.ProjectCharterSteeringComtProjectTeam;
import com.ais.innovationLab.api.entity.ResponseStatus;
import com.ais.innovationLab.api.service.ProjectCharterService;
import lombok.extern.slf4j.Slf4j;

@Validated
@Slf4j
@RestController
@RequestMapping("api/projectCharter")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProjectCharterController {

	@Autowired
	private ProjectCharterService projectCharterService;

	@RequestMapping(value = "/GetProjectCharterDetails", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<ProjectCharter>>> GetProjectCharterDetails() throws Exception {

		ResponseStatus<List<ProjectCharter>> responseStatus = new ResponseStatus<List<ProjectCharter>>();
		try {
			responseStatus = this.projectCharterService.GetProjectCharterDetailsService();
			return new ResponseEntity<ResponseStatus<List<ProjectCharter>>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in ProjectCharterController method GetProjectCharterDetails(): "
					+ e.getMessage());
			return new ResponseEntity<ResponseStatus<List<ProjectCharter>>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/GetProjectCharterByProjectIdDetails", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<ProjectCharter>>> GetProjectCharterByProjectIdDetails(int projectId)
			throws Exception {

		ResponseStatus<List<ProjectCharter>> responseStatus = new ResponseStatus<List<ProjectCharter>>();
		try {
			responseStatus = this.projectCharterService.GetProjectCharterByProjectIdService(projectId);
			return new ResponseEntity<ResponseStatus<List<ProjectCharter>>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterController method GetProjectCharterByProjectIdDetails(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<List<ProjectCharter>>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/GetProjectCharterResultArea", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<ProjectCharterResultArea>>> GetProjectCharterResultArea(
			@RequestParam int p_ProjectId, @RequestParam int p_ProjectTypeId) throws Exception {
		ResponseStatus<List<ProjectCharterResultArea>> responseStatus = new ResponseStatus<List<ProjectCharterResultArea>>();
		try {
			responseStatus = this.projectCharterService.GetProjectCharterResultAreaService(p_ProjectId,
					p_ProjectTypeId);
			return new ResponseEntity<ResponseStatus<List<ProjectCharterResultArea>>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterController method GetProjectCharterResultArea(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<List<ProjectCharterResultArea>>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/GetProjectCharterBenefits", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<ProjectCharterBenefits>>> GetProjectCharterBenefits(
			@RequestParam int p_ProjectId, @RequestParam int p_ProjectTypeId) throws Exception {
		ResponseStatus<List<ProjectCharterBenefits>> responseStatus = new ResponseStatus<List<ProjectCharterBenefits>>();
		try {
			responseStatus = this.projectCharterService.GetProjectCharterBenefitsService(p_ProjectId, p_ProjectTypeId);
			return new ResponseEntity<ResponseStatus<List<ProjectCharterBenefits>>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in ProjectCharterController method GetProjectCharterBenefits(): "
					+ e.getMessage());
			return new ResponseEntity<ResponseStatus<List<ProjectCharterBenefits>>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/GetProjectCharterSteeringComtProjectTeam", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<ProjectCharterSteeringComtProjectTeam>>> GetProjectCharterSteeringComtProjectTeam(
			@RequestParam int p_ProjectId, @RequestParam int p_ProjectTypeId) throws Exception {
		ResponseStatus<List<ProjectCharterSteeringComtProjectTeam>> responseStatus = new ResponseStatus<List<ProjectCharterSteeringComtProjectTeam>>();
		try {
			responseStatus = this.projectCharterService.GetProjectCharterSteeringComtProjectTeamService(p_ProjectId,
					p_ProjectTypeId);
			return new ResponseEntity<ResponseStatus<List<ProjectCharterSteeringComtProjectTeam>>>(responseStatus,
					(HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in ProjectCharterController method GetProjectCharterBenefits(): "
					+ e.getMessage());
			return new ResponseEntity<ResponseStatus<List<ProjectCharterSteeringComtProjectTeam>>>(
					HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "/GetProjectCharterDMAICPhase", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<ProjectCharterDmaicphaseToll>>> GetProjectCharterDMAICPhase(
			@RequestParam int p_ProjectId, @RequestParam int p_ProjectTypeId) throws Exception {
		ResponseStatus<List<ProjectCharterDmaicphaseToll>> responseStatus = new ResponseStatus<List<ProjectCharterDmaicphaseToll>>();
		try {
			responseStatus = this.projectCharterService.GetProjectCharterDMAICPhaseService(p_ProjectId,
					p_ProjectTypeId);
			return new ResponseEntity<ResponseStatus<List<ProjectCharterDmaicphaseToll>>>(responseStatus,
					(HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterController method GetProjectCharterDMAICPhase(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<List<ProjectCharterDmaicphaseToll>>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/GetProjectCharterSteeringCommitte", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<ProjectCharterSteeringCommitee>>> GetProjectCharterSteeringCommitte(
			@RequestParam int p_ProjectId, @RequestParam int p_ProjectTypeId) throws Exception {
		ResponseStatus<List<ProjectCharterSteeringCommitee>> responseStatus = new ResponseStatus<List<ProjectCharterSteeringCommitee>>();
		try {
			responseStatus = this.projectCharterService.GetProjectCharterSteeringCommitteService(p_ProjectId,
					p_ProjectTypeId);
			return new ResponseEntity<ResponseStatus<List<ProjectCharterSteeringCommitee>>>(responseStatus,
					(HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterController method GetProjectCharterSteeringCommitte(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<List<ProjectCharterSteeringCommitee>>>(HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "/GetProjectCharterKeyStakeHolders", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<ProjectCharterKeyStakeHolders>>> GetProjectCharterKeyStakeHolders(
			@RequestParam int p_ProjectId, @RequestParam int p_ProjectTypeId) throws Exception {
		ResponseStatus<List<ProjectCharterKeyStakeHolders>> responseStatus = new ResponseStatus<List<ProjectCharterKeyStakeHolders>>();
		try {
			responseStatus = this.projectCharterService.GetProjectCharterKeyStakeHoldersService(p_ProjectId,
					p_ProjectTypeId);
			return new ResponseEntity<ResponseStatus<List<ProjectCharterKeyStakeHolders>>>(responseStatus,
					(HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterController method GetProjectCharterKeyStakeHolders(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<List<ProjectCharterKeyStakeHolders>>>(HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "/GetProjectCharterDocuments", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<ProjectCharterDocuments>>> GetProjectCharterDocuments(
			@RequestParam int p_ProjectId, @RequestParam int p_ProjectTypeId) throws Exception {
		ResponseStatus<List<ProjectCharterDocuments>> responseStatus = new ResponseStatus<List<ProjectCharterDocuments>>();
		try {
			responseStatus = this.projectCharterService.GetProjectCharterDocumentsService(p_ProjectId, p_ProjectTypeId);
			return new ResponseEntity<ResponseStatus<List<ProjectCharterDocuments>>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterController method GetProjectCharterDocuments(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<List<ProjectCharterDocuments>>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/GetProjectCharterRivisionHistory", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<ProjectCharterRevisionHistory>>> GetProjectCharterRivisionHistory(
			@RequestParam int p_ProjectId, @RequestParam int p_ProjectTypeId) throws Exception {
		ResponseStatus<List<ProjectCharterRevisionHistory>> responseStatus = new ResponseStatus<List<ProjectCharterRevisionHistory>>();
		try {
			responseStatus = this.projectCharterService.GetProjectCharterRivisionHistoryService(p_ProjectId,
					p_ProjectTypeId);
			return new ResponseEntity<ResponseStatus<List<ProjectCharterRevisionHistory>>>(responseStatus,
					(HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterController method GetProjectCharterRivisionHistory(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<List<ProjectCharterRevisionHistory>>>(HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "/insertProjectCharterdetail", method = RequestMethod.POST)
	public ResponseEntity<ResponseStatus<String>> InsertProjectCharterdetail(@RequestBody ProjectCharter projectCharter)
			throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			responseStatus = this.projectCharterService.InsertProjectCharterdetailService(projectCharter);
			return new ResponseEntity<ResponseStatus<String>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterController method InsertProjectCharterdetail(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<String>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/updateProjectCharterdetail", method = RequestMethod.POST)
	public ResponseEntity<ResponseStatus<String>> UpdateProjectCharterdetail(@RequestBody ProjectCharter projectCharter)
			throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			responseStatus = this.projectCharterService.UpdateProjectCharterdetailService(projectCharter);
			return new ResponseEntity<ResponseStatus<String>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterController method UpdateProjectCharterdetail(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<String>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/insertProjectCharterResultAreaDetail", method = RequestMethod.POST)
	public ResponseEntity<ResponseStatus<String>> InsertProjectCharterResultAreaDetail(@RequestParam int projectId,
			@RequestParam int projectTypeId, @RequestBody List<ProjectCharterResultArea> projectCharterResultArea)
			throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			responseStatus = this.projectCharterService.InsertProjectCharterResultAreaDetailService(projectId,
					projectTypeId, projectCharterResultArea);
			return new ResponseEntity<ResponseStatus<String>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterController method InsertProjectCharterResultAreaDetail(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<String>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/insertProjectCharterDmaicphaseToll", method = RequestMethod.POST)
	public ResponseEntity<ResponseStatus<String>> InsertProjectCharterDmaicphaseToll(@RequestParam int projectId,
			@RequestParam int projectTypeId,
			@RequestBody List<ProjectCharterDmaicphaseToll> projectCharterDmaicphaseToll) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			responseStatus = this.projectCharterService.InsertProjectCharterDmaicphaseTollService(projectId,
					projectTypeId, projectCharterDmaicphaseToll);
			return new ResponseEntity<ResponseStatus<String>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterController method InsertProjectCharterDmaicphaseToll(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<String>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/insertProjectCharterSteeringCommittedetail", method = RequestMethod.POST)
	public ResponseEntity<ResponseStatus<String>> InsertProjectCharterSteeringCommitteDetail(
			@RequestParam int projectId, @RequestParam int projectTypeId,
			@RequestBody ProjectCharterSteeringCommitee projectCharterSteeringCommitee) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			responseStatus = this.projectCharterService.InsertProjectCharterSteeringCommitteDetailService(projectId,
					projectTypeId, projectCharterSteeringCommitee);
			return new ResponseEntity<ResponseStatus<String>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterController method InsertProjectCharterSteeringCommitteDetail(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<String>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/insertProjectCharterSteeringCommitteTeamdetail", method = RequestMethod.POST)
	public ResponseEntity<ResponseStatus<String>> InsertProjectCharterSteeringCommitteTeamDetail(
			@RequestParam int projectId, @RequestParam int projectTypeId,
			@RequestBody List<ProjectCharterSteeringComtProjectTeam> projectCharterSteeringComtProjectTeam)
			throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			responseStatus = this.projectCharterService.InsertProjectCharterSteeringCommitteTeamDetailService(projectId,
					projectTypeId, projectCharterSteeringComtProjectTeam);
			return new ResponseEntity<ResponseStatus<String>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterController method InsertProjectCharterSteeringCommitteTeamDetail(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<String>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/insertProjectCharterBenefitDetail", method = RequestMethod.POST)
	public ResponseEntity<ResponseStatus<String>> InsertProjectCharterBenefitDetail(@RequestParam int projectId,
			@RequestParam int projectTypeId, @RequestBody List<ProjectCharterBenefits> projectCharterBenefits)
			throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			responseStatus = this.projectCharterService.InsertProjectCharterBenefitDetailService(projectId,
					projectTypeId, projectCharterBenefits);
			return new ResponseEntity<ResponseStatus<String>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterController method InsertProjectCharterdetail(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<String>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/insertProjectCharterKeyStakeHolderdetail", method = RequestMethod.POST)
	public ResponseEntity<ResponseStatus<String>> InsertProjectCharterKeyStakeHolderdetail(@RequestParam int projectId,
			@RequestParam int projectTypeId,
			@RequestBody List<ProjectCharterKeyStakeHolders> projectCharterKeyStakeHolders) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			responseStatus = this.projectCharterService.InsertProjectCharterKeyStakeHolderdetailService(projectId,
					projectTypeId, projectCharterKeyStakeHolders);
			return new ResponseEntity<ResponseStatus<String>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterController method InsertProjectCharterKeyStakeHolderdetail(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<String>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/insertProjectCharterDocumentDetail", method = RequestMethod.POST)
	public ResponseEntity<ResponseStatus<String>> InsertProjectCharterDocumentDetail(@RequestParam int projectId,
			@RequestParam int projectTypeId, @RequestBody List<ProjectCharterDocuments> projectCharterDocuments)
			throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			responseStatus = this.projectCharterService.InsertProjectCharterDocumentDetailService(projectId,
					projectTypeId, projectCharterDocuments);
			return new ResponseEntity<ResponseStatus<String>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterController method InsertProjectCharterDocumentDetail(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<String>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/insertProjectCharterRevisionHistorydetail", method = RequestMethod.POST)
	public ResponseEntity<ResponseStatus<String>> InsertProjectCharterRevisionHistorydetail(@RequestParam int projectId,
			@RequestParam int projectTypeId,
			@RequestBody List<ProjectCharterRevisionHistory> projectCharterRevisionHistory) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			responseStatus = this.projectCharterService.InsertProjectCharterRevisionHistoryService(projectId,
					projectTypeId, projectCharterRevisionHistory);
			return new ResponseEntity<ResponseStatus<String>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterController method InsertProjectCharterRevisionHistorydetail(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<String>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/updateProjectCharterStatus", method = RequestMethod.POST)
	public ResponseEntity<ResponseStatus<String>> UpdateProjectCharterStatus(@RequestParam int projectId,
			@RequestParam int projectStatusId, @RequestParam String updatedBy, @RequestParam String comments)
			throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			responseStatus = this.projectCharterService.UpdateProjectCharterStatusService(projectId, updatedBy,
					comments, projectStatusId);
			return new ResponseEntity<ResponseStatus<String>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in ProjectCharterController method UpdateProjectCharterStatus(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<String>>(HttpStatus.BAD_REQUEST);
		}
	}

}