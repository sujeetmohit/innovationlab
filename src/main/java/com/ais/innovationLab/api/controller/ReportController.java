package com.ais.innovationLab.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.ais.innovationLab.api.entity.Report;
import com.ais.innovationLab.api.entity.ResponseStatus;
import com.ais.innovationLab.api.service.ReportService;
import lombok.extern.slf4j.Slf4j;

@Validated
@Slf4j
@RestController
@RequestMapping("api/report")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ReportController {

	@Autowired
	private ReportService reportService;

	@RequestMapping(value = "/getDashBoardDetails", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<Report>> GetDashBoardReport()
			throws Exception {
		ResponseStatus<Report> responseStatus = new ResponseStatus<Report>();
		try {
			responseStatus = this.reportService.GetDashBoardReportService();
			return new ResponseEntity<ResponseStatus<Report>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in ReportApiController method GetDashBoardReport(): "
					+ e.getMessage());
			return new ResponseEntity<ResponseStatus<Report>>(HttpStatus.BAD_REQUEST);
		}
	}

}
