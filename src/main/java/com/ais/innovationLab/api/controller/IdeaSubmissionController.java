package com.ais.innovationLab.api.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.ais.innovationLab.api.entity.IdeaSubmission;
import com.ais.innovationLab.api.entity.ResponseStatus;
import com.ais.innovationLab.api.service.IdeaSubmissionService;
import lombok.extern.slf4j.Slf4j;

@Validated
@Slf4j
@RestController
@RequestMapping("api/ideaSubmission")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class IdeaSubmissionController {

	@Autowired
	private IdeaSubmissionService ideaSubmissionService;

	@RequestMapping(value = "/getIdeaSubmissionDetails", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<IdeaSubmission>>> GetIdeaSubmissionList(@RequestParam String loginEmailId)
			throws Exception {

		ResponseStatus<List<IdeaSubmission>> responseStatus = new ResponseStatus<List<IdeaSubmission>>();
		try {
			responseStatus = this.ideaSubmissionService.GetIdeaSubmissionDetailsService(loginEmailId);
			return new ResponseEntity<ResponseStatus<List<IdeaSubmission>>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in IdeaSubmissionController method GetIdeaSubmissionList(): "
					+ e.getMessage());
			return new ResponseEntity<ResponseStatus<List<IdeaSubmission>>>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "/getIdeaSubmissionbyId", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<IdeaSubmission>>> GetIdeaSubmissionbyId(@RequestParam int ideaSubmissionId)
			throws Exception {

		ResponseStatus<List<IdeaSubmission>> responseStatus = new ResponseStatus<List<IdeaSubmission>>();
		try {
			responseStatus = this.ideaSubmissionService.GetIdeaSubmissionbyIdService(ideaSubmissionId);
			return new ResponseEntity<ResponseStatus<List<IdeaSubmission>>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in IdeaSubmissionController method GetIdeaSubmissionbyId(): "
					+ e.getMessage());
			return new ResponseEntity<ResponseStatus<List<IdeaSubmission>>>(HttpStatus.BAD_REQUEST);
		}
	}


	@RequestMapping(value = "/insertIdeaSubmissiondetail", method = RequestMethod.POST)
	public ResponseEntity<ResponseStatus<String>> InsertIdeaSubmissiondetail(@RequestBody IdeaSubmission ideaSubmission)
			throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			responseStatus = this.ideaSubmissionService.InsertIdeaSubmissiondetailService(ideaSubmission);
			return new ResponseEntity<ResponseStatus<String>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in IdeaSubmissionController method InsertIdeaSubmissiondetailService(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<String>>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "/updateIdeaSubmissiondetail", method = RequestMethod.POST)
	public ResponseEntity<ResponseStatus<String>> UpdateIdeaSubmissiondetail(@RequestBody IdeaSubmission ideaSubmission)
			throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			responseStatus = this.ideaSubmissionService.UpdateIdeaSubmissiondetailService(ideaSubmission);
			return new ResponseEntity<ResponseStatus<String>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in IdeaSubmissionController method UpdateIdeaSubmissiondetailService(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<String>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/updateManagerStatus", method = RequestMethod.POST)
	public ResponseEntity<ResponseStatus<String>> updateIdeaSubmissiondetail(@RequestParam int ideaSubmissionId,
			@RequestParam int managerStatusId, @RequestParam String managerFeedback) throws Exception {
		ResponseStatus<String> responseStatus = new ResponseStatus<String>();
		try {
			responseStatus = this.ideaSubmissionService.UpdateManagerStatusIdeaSubmissiondetailService(ideaSubmissionId,
					managerStatusId, managerFeedback);
			return new ResponseEntity<ResponseStatus<String>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in IdeaSubmissionController method UpdateIdeaSubmissiondetail(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<String>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/getApprovedIdeaSubmissionDetails", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<IdeaSubmission>>> GetApprovedIdeaSubmissionList(
			@RequestParam String loginEmailId, @RequestParam int managerStatusId) throws Exception {

		ResponseStatus<List<IdeaSubmission>> responseStatus = new ResponseStatus<List<IdeaSubmission>>();
		try {
			responseStatus = this.ideaSubmissionService.GetManagerStatusIdeaSubmissionDetailsService(loginEmailId,
					managerStatusId);
			return new ResponseEntity<ResponseStatus<List<IdeaSubmission>>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in IdeaSubmissionController method GetApprovedIdeaSubmissionDetailsService(): "
							+ e.getMessage());
			return new ResponseEntity<ResponseStatus<List<IdeaSubmission>>>(HttpStatus.BAD_REQUEST);
		}
	}
	
}
