package com.ais.innovationLab.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.multipart.MultipartFile;

import com.ais.innovationLab.api.entity.FileModel;
import com.ais.innovationLab.api.service.FileService;
import com.ais.innovationLab.api.entity.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

@Validated
@Slf4j
@RestController
@RequestMapping("api/fileupload")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UploaderController {

	@Autowired
	private FileService fileService;

	@SuppressWarnings({ "rawtypes", "unused" })
	@PostMapping("/ideaSubmission/uploadIdeaSubmission")
	public ResponseEntity<ResponseStatus> uplaodIdeaSubmission(@RequestParam("ideaSubmissionfile") MultipartFile file,
			@RequestParam("ideaSubmissionId") int ideaSubmissionId) throws Exception {

		ResponseStatus<String> resp = null;
		FileModel ideaSubmissionFile = new FileModel(file.getOriginalFilename(), file.getContentType(), file);
		if (ideaSubmissionFile != null) {
			resp = this.fileService.uplaodIdeaSubmissionService(ideaSubmissionFile, ideaSubmissionId);
		} else {
			log.info("IdeaSubmission Document not found");
		}
		return new ResponseEntity<ResponseStatus>(resp, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")

	@GetMapping("/ideaSubmission/deleteIdeaSubmission")
	public ResponseEntity<ResponseStatus> DeleteIdeaSubmission(int ideaSubmissionId) throws Exception {
		ResponseStatus<Integer> resp = null;
		if (ideaSubmissionId > 0) {
			resp = fileService.DeleteIdeaSubmissionService(ideaSubmissionId);
		} else {
			log.info("IdeaSubmission Id not found");
		}
		return new ResponseEntity<ResponseStatus>(resp, HttpStatus.OK);

	}

	@GetMapping("/ideaSubmission/downloadIdeaSubmission")
	public ResponseEntity<Object> DownloadIdeaSubmission(@RequestParam("ideaSubmissionId") int ideaSubmissionId,
			HttpServletRequest request) throws Exception {
		Resource resource = null;
		try {
			resource = this.fileService.DownloadIdeaSubmissionService(ideaSubmissionId);
		} catch (Exception e) {
			String error = "idea Submission File not found";
			return ResponseEntity.ok().body(error.toString());
		}
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (Exception ex) {
			log.info("Could not determine file type.");
		}
		// Fallback to the default content type if type could not be determined
		if (contentType == null) {
			contentType = "application/octet-stream";
		}
		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);

	}

}
