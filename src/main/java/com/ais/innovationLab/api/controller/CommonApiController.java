package com.ais.innovationLab.api.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ais.innovationLab.api.entity.DMAIC_Phase;
import com.ais.innovationLab.api.entity.LeverTechnology;
import com.ais.innovationLab.api.entity.ProjectStatus;
import com.ais.innovationLab.api.entity.ProjectType;
import com.ais.innovationLab.api.entity.ResponseStatus;
import com.ais.innovationLab.api.entity.ResultArea;
import com.ais.innovationLab.api.service.CommonService;
import lombok.extern.slf4j.Slf4j;

@Validated
@Slf4j
@RestController
@RequestMapping("api/common")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CommonApiController {

	@Autowired
	private CommonService commonService;

	@RequestMapping(value = "/getProjectType", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<ProjectType>>> GetProjectTypeDetail() throws Exception {

		ResponseStatus<List<ProjectType>> responseStatus = new ResponseStatus<List<ProjectType>>();
		try {
			responseStatus = this.commonService.GetProjectTypeDetailService();
			return new ResponseEntity<ResponseStatus<List<ProjectType>>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in CommonApiController method GetProjectTypeDetail(): "
					+ e.getMessage());
			return new ResponseEntity<ResponseStatus<List<ProjectType>>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/getProjectStatus", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<ProjectStatus>>> GetProjectStatusDetail() throws Exception {

		ResponseStatus<List<ProjectStatus>> responseStatus = new ResponseStatus<List<ProjectStatus>>();
		try {
			responseStatus = this.commonService.GetProjectStatusService();
			return new ResponseEntity<ResponseStatus<List<ProjectStatus>>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in CommonApiController method GetProjectStatusDetail(): "
					+ e.getMessage());
			return new ResponseEntity<ResponseStatus<List<ProjectStatus>>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/getLeverTechnology", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<LeverTechnology>>> GetLeverTechnologyDetail() throws Exception {

		ResponseStatus<List<LeverTechnology>> responseStatus = new ResponseStatus<List<LeverTechnology>>();
		try {
			responseStatus = this.commonService.GetLeverTechnologyDetailService();
			return new ResponseEntity<ResponseStatus<List<LeverTechnology>>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in CommonApiController method GetLeverTechnologyDetail(): "
					+ e.getMessage());
			return new ResponseEntity<ResponseStatus<List<LeverTechnology>>>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/getDMAICPhase", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<DMAIC_Phase>>> GetDMAICPhaseDetail() throws Exception {

		ResponseStatus<List<DMAIC_Phase>> responseStatus = new ResponseStatus<List<DMAIC_Phase>>();
		try {
			responseStatus = this.commonService.GetDMAIC_PhaseDetailService();
			return new ResponseEntity<ResponseStatus<List<DMAIC_Phase>>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in CommonApiController method DMAIC_PhaseDetail(): "
					+ e.getMessage());
			return new ResponseEntity<ResponseStatus<List<DMAIC_Phase>>>(HttpStatus.BAD_REQUEST);

		}
	}

	@RequestMapping(value = "/getResultAreaType", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<ResultArea>>> GetResultArea() throws Exception {

		ResponseStatus<List<ResultArea>> responseStatus = new ResponseStatus<List<ResultArea>>();
		try {
			responseStatus = this.commonService.GetResultAreaDetailService();
			return new ResponseEntity<ResponseStatus<List<ResultArea>>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(
					"exception occured while calling in CommonApiController method GetResultArea(): " + e.getMessage());
			return new ResponseEntity<ResponseStatus<List<ResultArea>>>(HttpStatus.BAD_REQUEST);

		}
	}
}