package com.ais.innovationLab.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ais.innovationLab.api.entity.ResultType;
import com.ais.innovationLab.api.entity.ResponseStatus;
import com.ais.innovationLab.api.service.ResultTypeSevice;

import lombok.extern.slf4j.Slf4j;

@Validated
@Slf4j
@RestController
@RequestMapping("api/ResultType")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ResultTypeController {
	@Autowired
	private ResultTypeSevice ResultTypeService;

	@RequestMapping(value = "/getResultTypeDetails", method = RequestMethod.GET)
	public ResponseEntity<ResponseStatus<List<ResultType>>> GetResultTypeList()
			throws Exception {

		ResponseStatus<List<ResultType>> responseStatus = new ResponseStatus<List<ResultType>>();
		try {
			responseStatus = this.ResultTypeService.GetResultTypedetailService();
			return new ResponseEntity<ResponseStatus<List<ResultType>>>(responseStatus, (HttpStatus.OK));
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception occured while calling in ResultTypeController method GetResultTypeList(): "
					+ e.getMessage());
			return new ResponseEntity<ResponseStatus<List<ResultType>>>(HttpStatus.BAD_REQUEST);
		}
	}
}
