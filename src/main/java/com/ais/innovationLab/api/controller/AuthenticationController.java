package com.ais.innovationLab.api.controller;

import static com.ais.innovationLab.api.security.JwtUtils.generateHMACToken;

import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ais.innovationLab.api.entity.ICustomUserDetails;
import com.ais.innovationLab.api.exceptions.UserNotActiveException;
import com.ais.innovationLab.api.security.LDAPAuthenticationProvider;
import com.ais.innovationLab.api.security.model.AuthenticationRequest;
import com.ais.innovationLab.api.security.model.AuthenticationResponse;
import com.ais.innovationLab.api.security.model.ResponseWrapper;
import com.ais.innovationLab.api.service.CustomUserDetailsService;
import com.nimbusds.jose.JOSEException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import lombok.AllArgsConstructor;
import lombok.Data;

@RestController
@RequestMapping("api/auth")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AuthenticationController {

	@Value("${app.jwtExpirationInMs}")
	private int expirationInMinutes;

	@Autowired
	private LDAPAuthenticationProvider ldapAuthenticationProvider;
	
	@SuppressWarnings("unused")
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	@SuppressWarnings("unused")
	@PostMapping("/authenticate")
	public AuthenticationResponse authenticateUser(@RequestBody AuthenticationRequest user) {
		AuthenticationResponse resp = new AuthenticationResponse();
		String username=user.getUsername().trim();
		JSONObject ldapResponse = ldapAuthenticationProvider.searchUser(username, user.getPassword().trim());
		String token = "";
		try {
			if (ldapResponse != null && ldapResponse.length() > 0) {

				ICustomUserDetails userDetails = this.customUserDetailsService.loadUserByUsername(user.getUsername());
				Authentication auth = new UsernamePasswordAuthenticationToken(userDetails.getUsername(),
						userDetails.getPassword(), userDetails.getAuthorities());
				SecurityContextHolder.getContext().setAuthentication(auth);
				token = this.generateAuthToken(auth, userDetails);
				resp.setToken(token);
				resp.setStatus(true);
				resp.setEmail(userDetails.getUsername());
				resp.setAis_id(userDetails.getId());
				resp.setAuthorities(userDetails.getAuthorities());
			} else {
				resp.setMessage("username/password is not correct");
				resp.setStatus(false);
			}

		} catch (UserNotActiveException e) {
			resp.setMessage(e.getMessage());
			resp.setStatus(false);
		} catch (UsernameNotFoundException e) {
			resp.setMessage("user/password is not correct");
			resp.setStatus(false);
		} catch (Exception e) {
			resp.setMessage("error=" + e.getMessage());
			resp.setStatus(false);
		}
		return (resp);
	}

	@GetMapping("/secureapi")
	public ResponseWrapper secureapi() {
		// fake class just for testing purpose
		@AllArgsConstructor
		@Data
		class SensitiveInformation {
			String foo;
			String bar;
		}

		return new ResponseWrapper(new SensitiveInformation("ABCD", "XYZ"), null, null);
	}

	private String generateAuthToken(Authentication auth, UserDetails user) throws IOException, JOSEException {
		String secret = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("secret.key"),
				Charset.defaultCharset());
		return generateHMACToken(user.getUsername(), auth.getAuthorities(), secret, this.expirationInMinutes);
	}
}
