package com.ais.innovationLab.api.repository;

import java.net.MalformedURLException;
import java.io.File;
import java.io.FileNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

import com.ais.innovationLab.api.entity.FileModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class FileRepositoryImpl implements FileRepository {

	@Value("${app.ideaSubmissionBasePath}")
	private String ideaSubmissionBasePath;
	
	@Autowired
	private EntityManager entityManager;

	@Override
	public String uplaodIdeaSubmissionRepo(FileModel file, int ideaSubmissionId) throws Exception {
		String result = "";
		try {
			if (!new File(this.ideaSubmissionBasePath).exists())
				new File(this.ideaSubmissionBasePath).mkdir();
			String ideaSubmissionDir = this.ideaSubmissionBasePath + ideaSubmissionId;
			if (!new File(ideaSubmissionDir).exists()) {
				new File(ideaSubmissionDir).mkdirs();
			}
			String filename = ideaSubmissionDir + "\\" + file.getName();;
			File dest = new File(filename);
			file.getMultipartFile().transferTo(dest);
			result = "Success";
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in FileRepositoryImpl=" + e.getMessage() + "");
			result = "failed";
			throw new Exception(
					"Exception occured while calling method uplaodIdeaSubmissionRepo() in FileRepositoryImpl.");
		}
		return result;
	}

	public Integer DeleteIdeaSubmissionRepo(int ideaSubmissionId) {
		String ideaSubmissionDir = this.ideaSubmissionBasePath + ideaSubmissionId;
		try {
			File dest = new File(ideaSubmissionDir);
			if (new File(ideaSubmissionDir).exists()) {
				File[] listFiles = dest.listFiles();
				for (File file : listFiles) {

					file.delete();
				}
				new File(ideaSubmissionDir).delete();
			}
			return 1;
		} catch (Exception e) {
			log.info("Could not Delete File in DeleteIdeaSubmissionRepo");
		}
		return 0;
	}

	public Resource DownloadIdeaSubmissionRepo(int ideaSubmissionId) throws Exception {
		Resource resource = null;
		try {
			String ideaSubmissionDir = this.ideaSubmissionBasePath + ideaSubmissionId;
			File dest = new File(ideaSubmissionDir);
			File[] listFiles = dest.listFiles();
			for (File file : listFiles) {
				Path filePath = Paths.get(file.getAbsolutePath()).normalize();
				resource = new UrlResource(filePath.toUri());
			}
			if (resource.exists()) {
				return resource;

			} else {
				throw new FileNotFoundException("File not found ");
			}
		} catch (MalformedURLException ex) {
			throw new FileNotFoundException("File not found ");
		}
	}

	public int UpdateReplaceIdeaSubmissionDocRepo(String ideasubmssionFileName, int ideaSubmissionId) throws Exception {
		int result = 0;

		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_UpdateIdeaSubmissionDocument");

			query.registerStoredProcedureParameter("p_IdeaSubmissionId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ReceiptFileName", String.class, ParameterMode.IN);

			query.setParameter("p_IdeaSubmissionId", ideaSubmissionId);
			query.setParameter("p_ReceiptFileName", ideasubmssionFileName);

			query.execute();
			result = 1;

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in FileRepositoryImpl=" + e.getMessage() + "");
			result = -1;
			throw new Exception(
					"Exception occured while calling method UpdateApprovedEmailSowNameRepo() in FileRepositoryImpl.");
		}
		return result;
	}
}
