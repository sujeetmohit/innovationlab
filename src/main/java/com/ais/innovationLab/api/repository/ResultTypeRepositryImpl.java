package com.ais.innovationLab.api.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ais.innovationLab.api.entity.ResultType;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class ResultTypeRepositryImpl implements ResultTypeRepository {
	@Autowired
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<ResultType> GetResultTypeRepo() throws Exception {
		List<ResultType> ResultType = null;

		try {

			StoredProcedureQuery query = this.entityManager.createStoredProcedureQuery("usp_GetResultTypeDetail",
					ResultType.class);

			ResultType = query.getResultList();

			if (ResultType != null && ResultType.size() > 0) {
				log.info("ResultType data found.");
			} else {
				log.info("ResultType data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ResultTypeRepositoryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetResultTypeRepo() in ResultTypeRepositoryImpl.");
		}
		return ResultType;
	}
}
