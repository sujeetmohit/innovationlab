package com.ais.innovationLab.api.repository;

import java.util.List;
import org.springframework.stereotype.Repository;
import com.ais.innovationLab.api.entity.IdeaSubmission;

@Repository
public interface IdeaSubmissionRepository {

	public List<IdeaSubmission> GetIdeaSubmissionDetailsRepo(String loginEmailId) throws Exception;
	
	public List<IdeaSubmission> GetIdeaSubmissionbyIdRepo(int ideaSubmissionId) throws Exception;

	public int InsertIdeaSubmissionDetailRepo(IdeaSubmission ideaSubmission) throws Exception;
	
	public int UpdateIdeaSubmissionDetailRepo(IdeaSubmission ideaSubmission) throws Exception;

	public int UpdateManagerStatusIdeaSubmissiondetailRepo(int ideaSubmissionId, int managerStatusId, String managerFeedback)
			throws Exception;
	
	public List<IdeaSubmission> GetManagerStatusIdeaSubmissionDetailsRepo(String loginEmailId,int managerStatusId) throws Exception;
	
	public int UpdateIdeaSubmissionFilePath(int id, String file) throws Exception;

}
