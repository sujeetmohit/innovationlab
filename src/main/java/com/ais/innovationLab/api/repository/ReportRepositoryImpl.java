package com.ais.innovationLab.api.repository;

import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.ais.innovationLab.api.entity.ReportIdeaDashboard;
import com.ais.innovationLab.api.entity.ReportProjectDashboard;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class ReportRepositoryImpl implements ReportRepository {

	@Autowired
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public ArrayList<ReportIdeaDashboard> GetIdeaReportRepo(int reportType) throws Exception {

		ArrayList<ReportIdeaDashboard> reportEmployee = new ArrayList<ReportIdeaDashboard>();

		try {

			StoredProcedureQuery query = this.entityManager.createStoredProcedureQuery("usp_GetDashboardReport",
					ReportIdeaDashboard.class);

			query.registerStoredProcedureParameter("p_ReportType", int.class, ParameterMode.IN);

			query.setParameter("p_ReportType", reportType);

			reportEmployee = (ArrayList<ReportIdeaDashboard>) query.getResultList();

			if (reportEmployee != null && reportEmployee.size() > 0) {
				log.info("DashBoard EmployeeReport data found.");
			} else {
				log.info("DashBoard EmployeeReport data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ReportRepositioryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetDashBoardReportRepo() in ReportRepositioryImpl.");
		}
		return reportEmployee;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<ReportProjectDashboard> GetProjectCharterReportRepo(int reportType) throws Exception {

		ArrayList<ReportProjectDashboard> requisitionDetail = new ArrayList<ReportProjectDashboard>();

		try {

			StoredProcedureQuery query = this.entityManager.createStoredProcedureQuery("usp_GetDashboardReport",
					ReportProjectDashboard.class);

			query.registerStoredProcedureParameter("p_ReportType", int.class, ParameterMode.IN);

			query.setParameter("p_ReportType", reportType);

			requisitionDetail = (ArrayList<ReportProjectDashboard>) query.getResultList();

			if (requisitionDetail != null && requisitionDetail.size() > 0) {
				log.info("DashBoard RequisitionReport data found.");
			} else {
				log.info("DashBoard RequisitionReport data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ReportRepositioryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetDashBoardReportRepo() in ReportRepositioryImpl.");
		}
		return requisitionDetail;
	}

}
