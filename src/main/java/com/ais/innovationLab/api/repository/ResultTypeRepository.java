package com.ais.innovationLab.api.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ais.innovationLab.api.entity.ResultType;

@Repository
public interface ResultTypeRepository {
	public List<ResultType> GetResultTypeRepo() throws Exception;

	//public int InsertResultTypeRepo() throws Exception;
}
