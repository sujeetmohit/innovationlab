package com.ais.innovationLab.api.repository;

import java.util.ArrayList;
import org.springframework.stereotype.Repository;
import com.ais.innovationLab.api.entity.ReportIdeaDashboard;
import com.ais.innovationLab.api.entity.ReportProjectDashboard;

@Repository
public interface ReportRepository {

	public ArrayList<ReportIdeaDashboard> GetIdeaReportRepo(int reportType) throws Exception;

	public ArrayList<ReportProjectDashboard> GetProjectCharterReportRepo(int reportType) throws Exception;

}
