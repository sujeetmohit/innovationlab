package com.ais.innovationLab.api.repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.ais.innovationLab.api.entity.IdeaSubmission;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class IdeaSubmissionRepositoryImpl implements IdeaSubmissionRepository {

	@Autowired
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<IdeaSubmission> GetIdeaSubmissionDetailsRepo(String loginEmailId) throws Exception {

		List<IdeaSubmission> ideaSubmissiondetail = null;

		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_GetIdeaSubmissionbyloginEmail", IdeaSubmission.class);

			query.registerStoredProcedureParameter("p_ownerreportingmanageremail", String.class, ParameterMode.IN);

			query.setParameter("p_ownerreportingmanageremail", loginEmailId);

			ideaSubmissiondetail = query.getResultList();

			if (ideaSubmissiondetail != null && ideaSubmissiondetail.size() > 0) {
				log.info("IdeaSubmission data found.");
			} else {
				log.info("IdeaSubmission data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in IdeaSubmissionRepositoryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetIdeaSubmissionDetailsRepo() in IdeaSubmissionRepositoryImpl.");
		}
		return ideaSubmissiondetail;
	}

	@SuppressWarnings("unchecked")
	public List<IdeaSubmission> GetIdeaSubmissionbyIdRepo(int ideaSubmissionId) throws Exception {

		List<IdeaSubmission> ideaSubmissiondetail = null;

		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_GetIdeaSubmissionbyId", IdeaSubmission.class);

			query.registerStoredProcedureParameter("p_IdeaSubmissionId", int.class, ParameterMode.IN);

			query.setParameter("p_IdeaSubmissionId", ideaSubmissionId);

			ideaSubmissiondetail = query.getResultList();

			if (ideaSubmissiondetail != null && ideaSubmissiondetail.size() > 0) {
				log.info("IdeaSubmission data found for Id = " + ideaSubmissionId);
			} else {
				log.info("IdeaSubmission data not found = " + ideaSubmissionId);
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in IdeaSubmissionRepositoryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetIdeaSubmissionbyIdRepo() in IdeaSubmissionRepositoryImpl.");
		}
		return ideaSubmissiondetail;
	}

	public int InsertIdeaSubmissionDetailRepo(IdeaSubmission ideaSubmission) throws Exception {
		int insertIdeaSubmissionResult = 0;

		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_InsertIdeaSubmissionDetail");

//			query.registerStoredProcedureParameter("p_Id", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("id", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("ownerempid", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("ownerofficialemail", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("OwnerreportingmanagerEmail", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("ideaownername", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("projectstatusid", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("designationname", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("designationid", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("client", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("departmentname", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("departmentid", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("hod", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("reportingManagerName", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("locationname", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("locationid", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("ideaproposalname", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("problemstatement", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("leverid", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("riskdescription", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("benefits", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("projectcommencementdate", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("projectcompletiondate", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("managerfeedback", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("receiptfilename", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("projectstatusidbymanager", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("projectstatusidbybtteam", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("btteamapprovalemail", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("createdby", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("updateby", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_IdeaSubmissionId", int.class, ParameterMode.OUT);
			
			///////////////// Assign Value/////////////////

//			query.setParameter("p_Id", ideaSubmission.getId());
			query.setParameter("id", ideaSubmission.getId());
			query.setParameter("ownerempid", ideaSubmission.getOwnerempid());
			query.setParameter("ownerofficialemail", ideaSubmission.getOwnerofficialemail());
			query.setParameter("OwnerreportingmanagerEmail", ideaSubmission.getOwnerreportingmanageremail());
			query.setParameter("ideaownername", ideaSubmission.getIdeaownername());
			query.setParameter("projectstatusid", ideaSubmission.getProjectstatusid());
			query.setParameter("designationname", ideaSubmission.getDesignationname());
			query.setParameter("designationid", ideaSubmission.getDesignationid());
			query.setParameter("client", ideaSubmission.getClient());
			query.setParameter("departmentname", ideaSubmission.getDepartmentname());
			query.setParameter("departmentid", ideaSubmission.getDepartmentid());
			query.setParameter("hod", ideaSubmission.getHod());
			query.setParameter("reportingManagerName", ideaSubmission.getReportingmanagerName());
			query.setParameter("locationname", ideaSubmission.getLocationname());
			query.setParameter("locationid", ideaSubmission.getLocationid());
			query.setParameter("ideaproposalname", ideaSubmission.getIdeaproposalname());
			query.setParameter("problemstatement", ideaSubmission.getProblemstatement());
			query.setParameter("leverid", ideaSubmission.getLeverid());
			query.setParameter("riskdescription", ideaSubmission.getRiskdescription());
			query.setParameter("benefits", ideaSubmission.getBenefits());
			query.setParameter("projectcommencementdate", ideaSubmission.getProjectcommencementdate());
			query.setParameter("projectcompletiondate", ideaSubmission.getProjectcommencementdate());
			query.setParameter("ProjectTypeId", ideaSubmission.getProjecttypeid());
			query.setParameter("managerfeedback", ideaSubmission.getManagerfeedback());
			query.setParameter("receiptfilename", ideaSubmission.getReceiptfilename());
			query.setParameter("projectstatusidbymanager", ideaSubmission.getProjectstatusid());
			query.setParameter("projectstatusidbybtteam", ideaSubmission.getProjectstatusid());
			query.setParameter("btteamapprovalemail", ideaSubmission.getBtteamapprovalemail());
			query.setParameter("createdby", ideaSubmission.getCreatedby());
			query.setParameter("updateby", ideaSubmission.getUpdateby());


			query.execute();
			insertIdeaSubmissionResult = (int) query.getOutputParameterValue("p_IdeaSubmissionId");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in IdeaSubmissionRepositoryImpl=" + e.getMessage() + "");
			insertIdeaSubmissionResult = -1;
			throw new Exception(
					"Exception occured while calling method InsertIdeaSubmissionDetailRepo() in IdeaSubmissionRepositoryImpl.");
		}
		return insertIdeaSubmissionResult;
	}

	public int UpdateManagerStatusIdeaSubmissiondetailRepo(int ideaSubmissionId, int managerStatusId,
			String managerFeedback) throws Exception {
		int insertUpdateIdeaSubmissionResult = 0;

		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_UpdateIdeaSubmissionStatusByManager");

			query.registerStoredProcedureParameter("p_IdeaSubmissionId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ManagerStatusId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ManagerFeedback", String.class, ParameterMode.IN);

			///////////////// Assign Value/////////////////

			query.setParameter("p_IdeaSubmissionId", ideaSubmissionId);
			query.setParameter("p_ManagerStatusId", managerStatusId);
			query.setParameter("p_ManagerFeedback", managerFeedback);

			query.execute();
			insertUpdateIdeaSubmissionResult = 1;

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in IdeaSubmissionRepositoryImpl=" + e.getMessage() + "");
			insertUpdateIdeaSubmissionResult = -1;
			throw new Exception(
					"Exception occured while calling method UpdateManagerStatusIdeaSubmissiondetailRepo() in IdeaSubmissionRepositoryImpl.");
		}
		return insertUpdateIdeaSubmissionResult;
	}

	@SuppressWarnings("unchecked")
	public List<IdeaSubmission> GetManagerStatusIdeaSubmissionDetailsRepo(String loginEmailId, int managerStatusId)
			throws Exception {

		List<IdeaSubmission> ideaSubmissiondetail = null;

		try {

			StoredProcedureQuery query = this.entityManager.createStoredProcedureQuery("usp_GetApprovedIdeaSubmission",
					IdeaSubmission.class);

			query.registerStoredProcedureParameter("p_loginEmail", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectStatusId", int.class, ParameterMode.IN);

			query.setParameter("p_loginEmail", loginEmailId);
			query.setParameter("p_ProjectStatusId", managerStatusId);

			ideaSubmissiondetail = query.getResultList();

			if (ideaSubmissiondetail != null && ideaSubmissiondetail.size() > 0) {
				log.info("IdeaSubmission data found.");
			} else {
				log.info("IdeaSubmission data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in IdeaSubmissionRepositoryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetManagerStatusIdeaSubmissionDetailsRepo() in IdeaSubmissionRepositoryImpl.");
		}
		return ideaSubmissiondetail;
	}

	@Override
	public int UpdateIdeaSubmissionFilePath(int id, String file) throws Exception {
		int result = 0;
		try {
			StoredProcedureQuery query = this.entityManager.createStoredProcedureQuery("usp_UpdateIdeaSubmissionDocument");
			query.registerStoredProcedureParameter("p_IdeaSubmissionId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ReceiptFileName", String.class, ParameterMode.IN);

			query.setParameter("p_IdeaSubmissionId", id);
			query.setParameter("p_ReceiptFileName", file);
			query.execute();

			result = 1;

		} catch (Exception ex) {
			log.error("Error in GetJobSourceDetail=" + ex.getMessage() + "");
			ex.printStackTrace();
			result = -1;
			throw new Exception(
					"Exception occured while calling method UpdateResumePath() in RecruitmentRepositoryImpl.");
		}
		return result;
	}

	@Override
	public int UpdateIdeaSubmissionDetailRepo(IdeaSubmission ideaSubmission) throws Exception {
		int updateIdeaSubmissionResult = 0;

		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_UpdateIdeaSubmissionDetails");

			
			query.registerStoredProcedureParameter("Id", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("ownerempid", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("ownerofficialemail", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("OwnerreportingmanagerEmail", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("ideaownername", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("projectstatusid", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("designationname", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("designationid", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("client", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("departmentname", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("departmentid", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("hod", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("reportingManagerName", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("locationname", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("locationid", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("ideaproposalname", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("problemstatement", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("leverid", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("riskdescription", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("benefits", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("projectcommencementdate", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("projectcompletiondate", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("managerfeedback", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("receiptfilename", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("projectstatusidbymanager", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("projectstatusidbybtteam", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("btteamapprovalemail", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("createdby", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("updateby", String.class, ParameterMode.IN);
			
			///////////////// Assign Value/////////////////

			query.setParameter("Id", ideaSubmission.getId());
			query.setParameter("ownerempid", ideaSubmission.getOwnerempid());
			query.setParameter("ownerofficialemail", ideaSubmission.getOwnerofficialemail());
			query.setParameter("OwnerreportingmanagerEmail", ideaSubmission.getOwnerreportingmanageremail());
			query.setParameter("ideaownername", ideaSubmission.getIdeaownername());
			query.setParameter("projectstatusid", ideaSubmission.getProjectstatusid());
			query.setParameter("designationname", ideaSubmission.getDesignationname());
			query.setParameter("designationid", ideaSubmission.getDesignationid());
			query.setParameter("client", ideaSubmission.getClient());
			query.setParameter("departmentname", ideaSubmission.getDepartmentname());
			query.setParameter("departmentid", ideaSubmission.getDepartmentid());
			query.setParameter("hod", ideaSubmission.getHod());
			query.setParameter("reportingManagerName", ideaSubmission.getReportingmanagerName());
			query.setParameter("locationname", ideaSubmission.getLocationname());
			query.setParameter("locationid", ideaSubmission.getLocationid());
			query.setParameter("ideaproposalname", ideaSubmission.getIdeaproposalname());
			query.setParameter("problemstatement", ideaSubmission.getProblemstatement());
			query.setParameter("leverid", ideaSubmission.getLeverid());
			query.setParameter("riskdescription", ideaSubmission.getRiskdescription());
			query.setParameter("benefits", ideaSubmission.getBenefits());
			query.setParameter("projectcommencementdate", ideaSubmission.getProjectcommencementdate());
			query.setParameter("projectcompletiondate", ideaSubmission.getProjectcommencementdate());
			query.setParameter("ProjectTypeId", ideaSubmission.getProjecttypeid());
			query.setParameter("managerfeedback", ideaSubmission.getManagerfeedback());
			query.setParameter("receiptfilename", ideaSubmission.getReceiptfilename());
			query.setParameter("projectstatusidbymanager", ideaSubmission.getProjectstatusid());
			query.setParameter("projectstatusidbybtteam", ideaSubmission.getProjectstatusid());
			query.setParameter("btteamapprovalemail", ideaSubmission.getBtteamapprovalemail());
			query.setParameter("createdby", ideaSubmission.getCreatedby());
			query.setParameter("updateby", ideaSubmission.getUpdateby());


			query.execute();
			updateIdeaSubmissionResult = 1;

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in IdeaSubmissionRepositoryImpl=" + e.getMessage() + "");
			updateIdeaSubmissionResult = -1;
			throw new Exception(
					"Exception occured while calling method UpdateIdeaSubmissionDetailRepo() in IdeaSubmissionRepositoryImpl.");
		}
		return updateIdeaSubmissionResult;
	}

}
