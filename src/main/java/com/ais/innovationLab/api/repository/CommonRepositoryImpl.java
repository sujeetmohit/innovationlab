package com.ais.innovationLab.api.repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ais.innovationLab.api.entity.DMAIC_Phase;
import com.ais.innovationLab.api.entity.LeverTechnology;
import com.ais.innovationLab.api.entity.ProjectStatus;
import com.ais.innovationLab.api.entity.ProjectType;
import com.ais.innovationLab.api.entity.ResultArea;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class CommonRepositoryImpl implements CommonRepository {

	@Autowired
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<ProjectType> GetProjectTypeRepo() throws Exception {

		List<ProjectType> projectTypedetail = null;

		try {

			StoredProcedureQuery query = this.entityManager.createStoredProcedureQuery("usp_GetProjectType",
					ProjectType.class);

			projectTypedetail = query.getResultList();

			if (projectTypedetail != null && projectTypedetail.size() > 0) {
				log.info("projectTypedetail data found.");
			} else {
				log.info("projectTypedetail data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in CommonRepositoryImpl=" + e.getMessage() + "");
			throw new Exception("Exception occured while calling method GetProjectTypeRepo() in CommonRepositoryImpl.");
		}
		return projectTypedetail;
	}

	@SuppressWarnings("unchecked")
	public List<ProjectStatus> GetProjectStatusRepo() throws Exception {

		List<ProjectStatus> projectStatus = null;

		try {

			StoredProcedureQuery query = this.entityManager.createStoredProcedureQuery("usp_GetProjectStatus",
					ProjectStatus.class);

			projectStatus = query.getResultList();

			if (projectStatus != null && projectStatus.size() > 0) {
				log.info("projectStatus data found.");
			} else {
				log.info("projectStatus data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in CommonRepositoryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetProjectStatusRepo() in CommonRepositoryImpl.");
		}
		return projectStatus;
	}

	@SuppressWarnings("unchecked")
	public List<LeverTechnology> GetLeverTechnologyRepo() throws Exception {

		List<LeverTechnology> leverTechnology = null;

		try {

			StoredProcedureQuery query = this.entityManager.createStoredProcedureQuery("usp_GetLeverTechnology",
					LeverTechnology.class);

			leverTechnology = query.getResultList();

			if (leverTechnology != null && leverTechnology.size() > 0) {
				log.info("leverTechnology data found.");
			} else {
				log.info("leverTechnology data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in CommonRepositoryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetLeverTechnologyRepo() in CommonRepositoryImpl.");
		}
		return leverTechnology;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DMAIC_Phase> GetDMAIC_PhaseRepo() throws Exception {
		List<DMAIC_Phase> dmaic_Phases = null;

		try {

			StoredProcedureQuery query = this.entityManager.createStoredProcedureQuery("usp_GetDMAIC_Phase",
					DMAIC_Phase.class);

			dmaic_Phases = query.getResultList();

			if (dmaic_Phases != null && dmaic_Phases.size() > 0) {
				log.info("dmaic_Phases data found.");
			} else {
				log.info("dmaic_Phase data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in CommonRepositoryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetDMAIC_PhasesRepo() in CommonRepositoryImpl.");
		}
		return dmaic_Phases;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ResultArea> GetResultAreaRepo() throws Exception {
		List<ResultArea> resultAreas = null;

		try {

			StoredProcedureQuery query = this.entityManager.createStoredProcedureQuery("usp_GetResultArea",
					ResultArea.class);

			resultAreas = query.getResultList();

			if (resultAreas != null && resultAreas.size() > 0) {
				log.info("ResultArea data found.");
			} else {
				log.info("ResultArea data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in CommonRepositoryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetResultAreaRepo() in CommonRepositoryImpl.");
		}
		return resultAreas;
	}

}
