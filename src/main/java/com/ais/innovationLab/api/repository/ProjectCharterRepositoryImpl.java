package com.ais.innovationLab.api.repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.ais.innovationLab.api.entity.ProjectCharter;
import com.ais.innovationLab.api.entity.ProjectCharterBenefits;
import com.ais.innovationLab.api.entity.ProjectCharterDmaicphaseToll;
import com.ais.innovationLab.api.entity.ProjectCharterDocuments;
import com.ais.innovationLab.api.entity.ProjectCharterKeyStakeHolders;
import com.ais.innovationLab.api.entity.ProjectCharterResultArea;
import com.ais.innovationLab.api.entity.ProjectCharterRevisionHistory;
import com.ais.innovationLab.api.entity.ProjectCharterSteeringCommitee;
import com.ais.innovationLab.api.entity.ProjectCharterSteeringComtProjectTeam;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@Transactional
public class ProjectCharterRepositoryImpl implements ProjectCharterRepository {

	@Autowired
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<ProjectCharter> GetProjectCharterDetailsRepo() throws Exception {

		List<ProjectCharter> projectCharterdetail = null;

		try {

			StoredProcedureQuery query = this.entityManager.createStoredProcedureQuery("usp_GetProjectCharterDetails",
					ProjectCharter.class);
			// query.registerStoredProcedureParameter(loginEmailId, String.class,
			// ParameterMode.IN);
			// query.setParameter("ownerreportingmanageremail", loginEmailId);

			projectCharterdetail = query.getResultList();

			if (projectCharterdetail != null && projectCharterdetail.size() > 0) {
				log.info("Project Charter data found.");
			} else {
				log.info("Project Charter data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterRepositoryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetProjectCharterDetailsRepo() in ProjectCharterRepositoryImpl.");
		}
		return projectCharterdetail;
	}

	@SuppressWarnings("unchecked")
	public List<ProjectCharter> GetProjectCharterByProjectIdRepo(int projectId) throws Exception {

		List<ProjectCharter> projectCharterdetail = null;

		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_GetProjectCharterDetailsbyId", ProjectCharter.class);

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);

			query.setParameter("p_ProjectId", projectId);

			projectCharterdetail = query.getResultList();

			if (projectCharterdetail != null && projectCharterdetail.size() > 0) {
				log.info("Project Charter data found.");
			} else {
				log.info("Project Charter data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterRepositoryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetProjectCharterByProjectIdRepo() in ProjectCharterRepositoryImpl.");
		}
		return projectCharterdetail;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProjectCharterResultArea> GetProjectCharterResultAreaRepo(int p_ProjectId, int p_ProjectTypeId)
			throws Exception {

		List<ProjectCharterResultArea> projectCharterResultAreaDetails = null;

		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_GetProjectCharterOtherDetails", ProjectCharterResultArea.class);

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_Status", int.class, ParameterMode.IN);

			query.setParameter("p_ProjectId", p_ProjectId);
			query.setParameter("p_Status", 8);
			query.setParameter("p_ProjectTypeId", p_ProjectTypeId);

			projectCharterResultAreaDetails = query.getResultList();

			if (projectCharterResultAreaDetails != null && projectCharterResultAreaDetails.size() > 0) {
				log.info("Project Charter data found.");
			} else {
				log.info("Project Charter data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterRepositoryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetProjectCharterOtherDetailsRepo() in ProjectCharterRepositoryImpl.");
		}
		return projectCharterResultAreaDetails;
	}

	@SuppressWarnings("unchecked")
	public List<ProjectCharterBenefits> GetProjectCharterBenefitsRepo(int p_ProjectId, int p_ProjectTypeId)
			throws Exception {
		List<ProjectCharterBenefits> projectCharterBenefitsDetails = null;
		try {
			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_GetProjectCharterOtherDetails", ProjectCharterBenefits.class);

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_Status", int.class, ParameterMode.IN);

			query.setParameter("p_ProjectId", p_ProjectId);
			query.setParameter("p_Status", 7);
			query.setParameter("p_ProjectTypeId", p_ProjectTypeId);

			projectCharterBenefitsDetails = query.getResultList();

			if (projectCharterBenefitsDetails != null && projectCharterBenefitsDetails.size() > 0) {
				log.info("Project Charter Benefit data found.");
			} else {
				log.info("Project Charter Benefit data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error In ProjectCharterRepositoryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetProjectCharterDocumentsRepo() in ProjectCharterRepositoryImpl.");
			// TODO: handle exception
		}
		return projectCharterBenefitsDetails;
	}

	@SuppressWarnings("unchecked")
	public List<ProjectCharterSteeringComtProjectTeam> GetProjectCharterSteeringComtProjectTeamRepo(int p_ProjectId,
			int p_ProjectTypeId) throws Exception {

		List<ProjectCharterSteeringComtProjectTeam> projectCharterSteeringComtProjectTeamDetails = null;

		try {
			StoredProcedureQuery query = this.entityManager.createStoredProcedureQuery(
					"usp_GetProjectCharterOtherDetails", ProjectCharterSteeringComtProjectTeam.class);

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_Status", int.class, ParameterMode.IN);

			query.setParameter("p_ProjectId", p_ProjectId);
			query.setParameter("p_Status", 3);
			query.setParameter("p_ProjectTypeId", p_ProjectTypeId);

			projectCharterSteeringComtProjectTeamDetails = query.getResultList();

			if (projectCharterSteeringComtProjectTeamDetails != null
					&& projectCharterSteeringComtProjectTeamDetails.size() > 0) {
				log.info("Project Charter SteeringComtProjectTeam data found.");
			} else {
				log.info("Project Charter SteeringComtProjectTeam data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error In ProjectCharterRepositoryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetProjectCharterDocumentsRepo() in ProjectCharterRepositoryImpl.");
			// TODO: handle exception
		}
		return projectCharterSteeringComtProjectTeamDetails;
	}

	@SuppressWarnings("unchecked")
	public List<ProjectCharterDmaicphaseToll> GetProjectCharterDMAICPhaseRepo(int p_ProjectId, int p_ProjectTypeId)
			throws Exception {
		List<ProjectCharterDmaicphaseToll> projectCharterDmaicphaseTollDetails = null;

		try {

			StoredProcedureQuery query = this.entityManager.createStoredProcedureQuery(
					"usp_GetProjectCharterOtherDetails", ProjectCharterDmaicphaseToll.class);

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_Status", int.class, ParameterMode.IN);

			query.setParameter("p_ProjectId", p_ProjectId);
			query.setParameter("p_Status", 1);
			query.setParameter("p_ProjectTypeId", p_ProjectTypeId);

			projectCharterDmaicphaseTollDetails = query.getResultList();

			if (projectCharterDmaicphaseTollDetails != null && projectCharterDmaicphaseTollDetails.size() > 0) {
				log.info("projectCharterSteeringCommiteeDetails data found.");
			} else {
				log.info("projectCharterSteeringCommiteeDetails data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterSteeringComtProjectTeamRepositoryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetProjectCharterSteeringComtProjectTeamRepo() in ProjectCharterSteeringComtProjectTeamRepositoryImpl.");
		}
		return projectCharterDmaicphaseTollDetails;

	}

	@SuppressWarnings("unchecked")
	public List<ProjectCharterSteeringCommitee> GetProjectCharterSteeringCommitteRepo(int p_ProjectId,
			int p_ProjectTypeId) throws Exception {
		List<ProjectCharterSteeringCommitee> projectCharterSteeringCommiteeDetails = null;

		try {

			StoredProcedureQuery query = this.entityManager.createStoredProcedureQuery(
					"usp_GetProjectCharterOtherDetails", ProjectCharterSteeringCommitee.class);

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_Status", int.class, ParameterMode.IN);

			query.setParameter("p_ProjectId", p_ProjectId);
			query.setParameter("p_Status", 2);
			query.setParameter("p_ProjectTypeId", p_ProjectTypeId);

			projectCharterSteeringCommiteeDetails = query.getResultList();

			if (projectCharterSteeringCommiteeDetails != null && projectCharterSteeringCommiteeDetails.size() > 0) {
				log.info("projectCharterSteeringCommiteeDetails data found.");
			} else {
				log.info("projectCharterSteeringCommiteeDetails data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterSteeringComtProjectTeamRepositoryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetProjectCharterSteeringComtProjectTeamRepo() in ProjectCharterSteeringComtProjectTeamRepositoryImpl.");
		}
		return projectCharterSteeringCommiteeDetails;

	}

	@SuppressWarnings("unchecked")
	public List<ProjectCharterKeyStakeHolders> GetProjectCharterKeyStakeHoldersRepo(int p_ProjectId,
			int p_ProjectTypeId) throws Exception {
		List<ProjectCharterKeyStakeHolders> projectCharterKeyStakeHoldersDetails = null;
		try {

			StoredProcedureQuery query = this.entityManager.createStoredProcedureQuery(
					"usp_GetProjectCharterOtherDetails", ProjectCharterKeyStakeHolders.class);

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_Status", int.class, ParameterMode.IN);

			query.setParameter("p_ProjectId", p_ProjectId);
			query.setParameter("p_Status", 4);
			query.setParameter("p_ProjectTypeId", p_ProjectTypeId);

			projectCharterKeyStakeHoldersDetails = query.getResultList();

			if (projectCharterKeyStakeHoldersDetails != null && projectCharterKeyStakeHoldersDetails.size() > 0) {
				log.info("ProjectCharterStreeingCommitee data found.");
			} else {
				log.info("ProjectCharterStreeingCommitee data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterSteeringCommiteesRepositoryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetProjectCharterSteeringCommiteesRepo() in ProjectCharterSteeringComtProjectTeamRepositoryImpl. ");

			// TODO: handle exception
		}
		// TODO Auto-generated method stub
		return projectCharterKeyStakeHoldersDetails;
	}

	@SuppressWarnings("unchecked")
	public List<ProjectCharterDocuments> GetProjectCharterDocumentsRepo(int p_ProjectId, int p_ProjectTypeId)
			throws Exception {
		List<ProjectCharterDocuments> projectCharterDocumentsDetails = null;
		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_GetProjectCharterOtherDetails", ProjectCharterDocuments.class);

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_Status", int.class, ParameterMode.IN);

			query.setParameter("p_ProjectId", p_ProjectId);
			query.setParameter("p_Status", 5);
			query.setParameter("p_ProjectTypeId", p_ProjectTypeId);

			projectCharterDocumentsDetails = query.getResultList();

			if (projectCharterDocumentsDetails != null && projectCharterDocumentsDetails.size() > 0) {
				log.info("projectCharterDocumentsDetails data found.");
			} else {
				log.info("projectCharterDocumentsDetails data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterSteeringCommiteesRepositoryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetProjectCharterSteeringCommiteesRepo() in ProjectCharterSteeringComtProjectTeamRepositoryImpl. ");

			// TODO: handle exception
		}
		// TODO Auto-generated method stub
		return projectCharterDocumentsDetails;
	}

	@SuppressWarnings("unchecked")
	public List<ProjectCharterRevisionHistory> GetProjectCharterRivisionHistoryRepo(int p_ProjectId,
			int p_ProjectTypeId) throws Exception {
		List<ProjectCharterRevisionHistory> projectCharterRevisionHistory = null;
		try {

			StoredProcedureQuery query = this.entityManager.createStoredProcedureQuery(
					"usp_GetProjectCharterOtherDetails", ProjectCharterRevisionHistory.class);

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_Status", int.class, ParameterMode.IN);

			query.setParameter("p_ProjectId", p_ProjectId);
			query.setParameter("p_Status", 6);
			query.setParameter("p_ProjectTypeId", p_ProjectTypeId);

			projectCharterRevisionHistory = query.getResultList();

			if (projectCharterRevisionHistory != null && projectCharterRevisionHistory.size() > 0) {
				log.info("projectCharterRevisionHistory data found.");
			} else {
				log.info("projectCharterRevisionHistory data not found.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterSteeringCommiteesRepositoryImpl=" + e.getMessage() + "");
			throw new Exception(
					"Exception occured while calling method GetProjectCharterRivisionHistoryRepo() in ProjectCharterSteeringComtProjectTeamRepositoryImpl. ");

			// TODO: handle exception
		}
		// TODO Auto-generated method stub
		return projectCharterRevisionHistory;
	}

	public int InsertProjectCharterdetailRepo(ProjectCharter projectCharter) throws Exception {

		int insertProjectCharterResult = 0;

		try {

			StoredProcedureQuery query = this.entityManager.createStoredProcedureQuery("usp_InsertProjectCharter");

			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_IdeaSubmissionId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectName", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectSponsor", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectManager", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectapprovalDate", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_LastrevisionDate", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProblemStatement", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectDescription", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_InScope", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_OutScope", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_LeverId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectDeliverable", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_BuisnessCase", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectRisk", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_Comments", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_CreatedBy", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.OUT);

			///////////////// Assign Value/////////////////

			query.setParameter("p_ProjectTypeId", projectCharter.getProjecttypeId());
			query.setParameter("p_IdeaSubmissionId", projectCharter.getIdeasubmissionId());
			query.setParameter("p_ProjectName", projectCharter.getProjectName());
			query.setParameter("p_ProjectSponsor", projectCharter.getProjectSponsor());
			query.setParameter("p_ProjectManager", projectCharter.getProjectManager());
			query.setParameter("p_ProjectapprovalDate", projectCharter.getProjectapprovalDate());
			query.setParameter("p_LastrevisionDate", projectCharter.getLastrevisionDate());
			query.setParameter("p_ProblemStatement", projectCharter.getProblemStatement());
			query.setParameter("p_ProjectDescription", projectCharter.getProjectDescription());
			query.setParameter("p_InScope", projectCharter.getInScope());
			query.setParameter("p_OutScope", projectCharter.getOutScope());
			query.setParameter("p_LeverId", projectCharter.getLeverId());
			query.setParameter("p_ProjectDeliverable", projectCharter.getProjectDeliverable());
			query.setParameter("p_BuisnessCase", projectCharter.getBuisnessCase());
			query.setParameter("p_ProjectRisk", projectCharter.getProjectRisk());
			query.setParameter("p_Comments", projectCharter.getComments());
			query.setParameter("p_CreatedBy", projectCharter.getCreatedBy());

			query.execute();
			insertProjectCharterResult = (int) query.getOutputParameterValue("p_ProjectId");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterRepositoryImpl=" + e.getMessage() + "");
			insertProjectCharterResult = -1;
			throw new Exception(
					"Exception occured while calling method InsertupdateProjectCharterdetailRepo() in ProjectCharterRepositoryImpl.");
		}
		return insertProjectCharterResult;
	}

	public int UpdateProjectCharterdetailRepo(ProjectCharter projectCharter) throws Exception {
		int updateProjectCharterResult = 0;

		try {

			StoredProcedureQuery query = this.entityManager.createStoredProcedureQuery("usp_UpdateProjectCharter");

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectName", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectSponsor", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectManager", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectapprovalDate", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_LastrevisionDate", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProblemStatement", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectDescription", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_InScope", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_OutScope", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_LeverId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectDeliverable", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_BuisnessCase", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectRisk", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_Comments", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_CreatedBy", String.class, ParameterMode.IN);

			///////////////// Assign Value/////////////////

			query.setParameter("p_ProjectId", projectCharter.getId());
			query.setParameter("p_ProjectTypeId", projectCharter.getProjecttypeId());
			query.setParameter("p_ProjectName", projectCharter.getProjectName());
			query.setParameter("p_ProjectSponsor", projectCharter.getProjectSponsor());
			query.setParameter("p_ProjectManager", projectCharter.getProjectManager());
			query.setParameter("p_ProjectapprovalDate", projectCharter.getProjectapprovalDate());
			query.setParameter("p_LastrevisionDate", projectCharter.getLastrevisionDate());
			query.setParameter("p_ProblemStatement", projectCharter.getProblemStatement());
			query.setParameter("p_ProjectDescription", projectCharter.getProjectDescription());
			query.setParameter("p_InScope", projectCharter.getInScope());
			query.setParameter("p_OutScope", projectCharter.getOutScope());
			query.setParameter("p_LeverId", projectCharter.getLeverId());
			query.setParameter("p_ProjectDeliverable", projectCharter.getProjectDeliverable());
			query.setParameter("p_BuisnessCase", projectCharter.getBuisnessCase());
			query.setParameter("p_ProjectRisk", projectCharter.getProjectRisk());
			query.setParameter("p_Comments", projectCharter.getComments());
			query.setParameter("p_CreatedBy", projectCharter.getCreatedBy());

			query.execute();
			updateProjectCharterResult = 1;

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterRepositoryImpl=" + e.getMessage() + "");
			updateProjectCharterResult = -1;
			throw new Exception(
					"Exception occured while calling method UpdateProjectCharterdetailRepo() in ProjectCharterRepositoryImpl.");
		}
		return updateProjectCharterResult;

	}

	public int InsertProjectCharterResultAreaDetailRepo(int projectId, int projectTypeId,
			ProjectCharterResultArea projectCharterResultArea) throws Exception {

		int insertProjectCharterResult = 0;

		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_InsertProjecChartertResultArea");

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ResultAreaId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_Description", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_CreatedBy", String.class, ParameterMode.IN);

			///////////////// Assign Value/////////////////

			query.setParameter("p_ProjectId", projectId);
			query.setParameter("p_ProjectTypeId", projectTypeId);
			query.setParameter("p_ResultAreaId", projectCharterResultArea.getResultareaId());
			query.setParameter("p_Description", projectCharterResultArea.getDescription());
			query.setParameter("p_CreatedBy", projectCharterResultArea.getCreatedBy());

			query.execute();
			insertProjectCharterResult = 1;

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterRepositoryImpl=" + e.getMessage() + "");
			insertProjectCharterResult = -1;
			throw new Exception(
					"Exception occured while calling method InsertProjectCharterResultAreaDetailRepo() in ProjectCharterRepositoryImpl.");
		}
		return insertProjectCharterResult;
	}

	public int InsertProjectCharterDmaicphaseTollRepo(int projectId, int projectTypeId,
			ProjectCharterDmaicphaseToll projectCharterDmaicphaseToll) throws Exception {

		int insertProjectCharterResult = 0;

		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_InsertProjectCharterDmaicphaseToll");

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_DmaicId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_StartDate", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_EndDate", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_TollGateReviewDate", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_CreatedBy", String.class, ParameterMode.IN);

			///////////////// Assign Value/////////////////

			query.setParameter("p_ProjectId", projectId);
			query.setParameter("p_ProjectTypeId", projectTypeId);
			query.setParameter("p_DmaicId", projectCharterDmaicphaseToll.getDmaicId());
			query.setParameter("p_StartDate", projectCharterDmaicphaseToll.getStartDate());
			query.setParameter("p_EndDate", projectCharterDmaicphaseToll.getEndDate());
			query.setParameter("p_TollGateReviewDate", projectCharterDmaicphaseToll.getTollgatereviewDate());
			query.setParameter("p_CreatedBy", projectCharterDmaicphaseToll.getCreatedBy());

			query.execute();
			insertProjectCharterResult = 1;

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterRepositoryImpl=" + e.getMessage() + "");
			insertProjectCharterResult = -1;
			throw new Exception(
					"Exception occured while calling method InsertProjectCharterDmaicphaseTollRepo() in ProjectCharterRepositoryImpl.");
		}
		return insertProjectCharterResult;
	}

	public int InsertProjectCharterSteeringCommitteRepo(int projectId, int projectTypeId,
			ProjectCharterSteeringCommitee projectCharterSteeringCommitee) throws Exception {

		int insertProjectCharterResult = 0;

		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_InsertProjectCharterSteeringCommitee");

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_Champion", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_MasterBlackBelt", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_BlackBelt", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_CreatedBy", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_SteeringId", int.class, ParameterMode.OUT);

			///////////////// Assign Value/////////////////

			query.setParameter("p_ProjectId", projectId);
			query.setParameter("p_ProjectTypeId", projectTypeId);
			query.setParameter("p_Champion", projectCharterSteeringCommitee.getChampion());
			query.setParameter("p_MasterBlackBelt", projectCharterSteeringCommitee.getMasterblackBelt());
			query.setParameter("p_BlackBelt", projectCharterSteeringCommitee.getBlackBelt());
			query.setParameter("p_CreatedBy", projectCharterSteeringCommitee.getCreatedBy());

			query.execute();
			insertProjectCharterResult = (int) query.getOutputParameterValue("p_SteeringId");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterRepositoryImpl=" + e.getMessage() + "");
			insertProjectCharterResult = -1;
			throw new Exception(
					"Exception occured while calling method InsertProjectCharterSteeringCommitteRepo() in ProjectCharterRepositoryImpl.");
		}
		return insertProjectCharterResult;
	}

	public int InsertProjectCharterSteeringCommitteTeamRepo(int projectId, int projectTypeId,
			ProjectCharterSteeringComtProjectTeam projectCharterSteeringComtProjectTeam) throws Exception {

		int insertProjectCharterTeamResult = 0;

		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_InsertProjectCharterSteeringComtProjectTeam");

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_SteeringId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_EmployeeId", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_EmployeeName", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_CreatedBy", String.class, ParameterMode.IN);

			///////////////// Assign Value/////////////////

			query.setParameter("p_ProjectId", projectId);
			query.setParameter("p_ProjectTypeId", projectTypeId);
			query.setParameter("p_SteeringId", projectCharterSteeringComtProjectTeam.getSteeringId());
			query.setParameter("p_EmployeeId", projectCharterSteeringComtProjectTeam.getEmployeeId());
			query.setParameter("p_EmployeeName", projectCharterSteeringComtProjectTeam.getEmployeeName());
			query.setParameter("p_CreatedBy", projectCharterSteeringComtProjectTeam.getCreatedBy());

			query.execute();
			insertProjectCharterTeamResult = 1;

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterRepositoryImpl=" + e.getMessage() + "");
			insertProjectCharterTeamResult = -1;
			throw new Exception(
					"Exception occured while calling method InsertProjectCharterSteeringCommitteTeamRepo() in ProjectCharterRepositoryImpl.");
		}
		return insertProjectCharterTeamResult;
	}

	public int InsertProjectCharterBenefitDetailRepo(int projectId, int projectTypeId,
			ProjectCharterBenefits projectCharterBenefits) throws Exception {

		int insertProjectCharterResult = 0;

		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_InsertProjecChartertBenefits");

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_Kpi", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_BaseLine", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_Goal", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_CreatedBy", String.class, ParameterMode.IN);

			///////////////// Assign Value/////////////////

			query.setParameter("p_ProjectId", projectId);
			query.setParameter("p_ProjectTypeId", projectTypeId);
			query.setParameter("p_Kpi", projectCharterBenefits.getKpi());
			query.setParameter("p_BaseLine", projectCharterBenefits.getBaseline());
			query.setParameter("p_Goal", projectCharterBenefits.getGoal());
			query.setParameter("p_CreatedBy", projectCharterBenefits.getCreatedBy());

			query.execute();
			insertProjectCharterResult = 1;

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterRepositoryImpl=" + e.getMessage() + "");
			insertProjectCharterResult = -1;
			throw new Exception(
					"Exception occured while calling method InsertProjectCharterBenefitDetailRepo() in ProjectCharterRepositoryImpl.");
		}
		return insertProjectCharterResult;
	}

	public int InsertProjectCharterKeyStakeHolderdetailRepo(int projectId, int projectTypeId,
			ProjectCharterKeyStakeHolders projectCharterKeyStakeHolders) throws Exception {

		int insertProjectCharterResult = 0;

		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_InsertProjectCharterKeyStakeHolders");

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_KsName", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_SuccessCriteria", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_CreatedBy", String.class, ParameterMode.IN);

			///////////////// Assign Value/////////////////

			query.setParameter("p_ProjectId", projectId);
			query.setParameter("p_ProjectTypeId", projectTypeId);
			query.setParameter("p_KsName", projectCharterKeyStakeHolders.getKsName());
			query.setParameter("p_SuccessCriteria", projectCharterKeyStakeHolders.getSuccessCriteria());
			query.setParameter("p_CreatedBy", projectCharterKeyStakeHolders.getCreatedBy());

			query.execute();
			insertProjectCharterResult = 1;

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterRepositoryImpl=" + e.getMessage() + "");
			insertProjectCharterResult = -1;
			throw new Exception(
					"Exception occured while calling method InsertProjectCharterKeyStakeHolderdetailRepo() in ProjectCharterRepositoryImpl.");
		}
		return insertProjectCharterResult;
	}

	public int InsertProjectCharterDocumentDetailRepo(int projectId, int projectTypeId,
			ProjectCharterDocuments projectCharterDocuments) throws Exception {

		int insertProjectCharterResult = 0;

		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_InsertProjectCharterDocuments");

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_DocumentName", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_DocumentNo", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_Resp", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_RequestedOn", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_UpdatedOn", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_RespRepository", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_CreatedBy", String.class, ParameterMode.IN);

			///////////////// Assign Value/////////////////

			query.setParameter("p_ProjectId", projectId);
			query.setParameter("p_ProjectTypeId", projectTypeId);
			query.setParameter("p_DocumentName", projectCharterDocuments.getDocumentName());
			query.setParameter("p_DocumentNo", projectCharterDocuments.getDocumentNo());
			query.setParameter("p_Resp", projectCharterDocuments.getResp());
			query.setParameter("p_RequestedOn", projectCharterDocuments.getRequestedOn());
			query.setParameter("p_UpdatedOn", projectCharterDocuments.getUpdatedOn());
			query.setParameter("p_RespRepository", projectCharterDocuments.getRespRepository());
			query.setParameter("p_CreatedBy", projectCharterDocuments.getCreatedBy());

			query.execute();
			insertProjectCharterResult = 1;

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterRepositoryImpl=" + e.getMessage() + "");
			insertProjectCharterResult = -1;
			throw new Exception(
					"Exception occured while calling method InsertProjectCharterDocumentDetailRepo() in ProjectCharterRepositoryImpl.");
		}
		return insertProjectCharterResult;
	}

	public int InsertProjectCharterRevisionHistoryRepo(int projectId, int projectTypeId,
			ProjectCharterRevisionHistory projectCharterRevisionHistory) throws Exception {

		int insertProjectCharterResult = 0;

		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_InsertProjectCharterRevisionHistory");

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_Description", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_DateModified", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_CheckedBy", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ApprovedBy", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_CreatedBy", String.class, ParameterMode.IN);

			///////////////// Assign Value/////////////////

			query.setParameter("p_ProjectId", projectId);
			query.setParameter("p_ProjectTypeId", projectTypeId);
			query.setParameter("p_Description", projectCharterRevisionHistory.getDescription());
			query.setParameter("p_DateModified", projectCharterRevisionHistory.getDateModified());
			query.setParameter("p_CheckedBy", projectCharterRevisionHistory.getCheckedBy());
			query.setParameter("p_ApprovedBy", projectCharterRevisionHistory.getApprovedBy());
			query.setParameter("p_CreatedBy", projectCharterRevisionHistory.getCreatedBy());

			query.execute();
			insertProjectCharterResult = 1;

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterRepositoryImpl=" + e.getMessage() + "");
			insertProjectCharterResult = -1;
			throw new Exception(
					"Exception occured while calling method InsertProjectCharterRevisionHistoryRepo() in ProjectCharterRepositoryImpl.");
		}
		return insertProjectCharterResult;
	}

	public int DeletePreviousProjectCharterDetailRepo(int projectId, int projectTypeId) throws Exception {

		int insertProjectCharterResult = 0;

		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_DeleteProjectCharterOtherDetails");

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectTypeId", int.class, ParameterMode.IN);

			///////////////// Assign Value/////////////////

			query.setParameter("p_ProjectId", projectId);
			query.setParameter("p_ProjectTypeId", projectTypeId);

			query.execute();
			insertProjectCharterResult = 1;

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterRepositoryImpl=" + e.getMessage() + "");
			insertProjectCharterResult = -1;
			throw new Exception(
					"Exception occured while calling method InsertProjectCharterRevisionHistoryRepo() in ProjectCharterRepositoryImpl.");
		}
		return insertProjectCharterResult;
	}

	public int UpdateProjectCharterStatusRepo(int projectId, String updatedBy, String comments, int projectStatusId)
			throws Exception {

		int insertProjectCharterResult = 0;

		try {

			StoredProcedureQuery query = this.entityManager
					.createStoredProcedureQuery("usp_UpdateProjectCharterStatus");

			query.registerStoredProcedureParameter("p_ProjectId", int.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_StatusUpdatedBy", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_Comments", String.class, ParameterMode.IN);
			query.registerStoredProcedureParameter("p_ProjectStatusId", int.class, ParameterMode.IN);

			///////////////// Assign Value/////////////////

			query.setParameter("p_ProjectId", projectId);
			query.setParameter("p_StatusUpdatedBy", updatedBy);
			query.setParameter("p_Comments", comments);
			query.setParameter("p_ProjectStatusId", projectStatusId);

			query.execute();
			insertProjectCharterResult = 1;

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in ProjectCharterRepositoryImpl=" + e.getMessage() + "");
			insertProjectCharterResult = -1;
			throw new Exception(
					"Exception occured while calling method UpdateProjectCharterStatusRepo() in ProjectCharterRepositoryImpl.");
		}
		return insertProjectCharterResult;
	}

}
