package com.ais.innovationLab.api.repository;

import java.util.List;
import org.springframework.stereotype.Repository;

import com.ais.innovationLab.api.entity.DMAIC_Phase;
import com.ais.innovationLab.api.entity.LeverTechnology;
import com.ais.innovationLab.api.entity.ProjectStatus;
import com.ais.innovationLab.api.entity.ProjectType;
import com.ais.innovationLab.api.entity.ResultArea;

@Repository
public interface CommonRepository {

	public List<ProjectType> GetProjectTypeRepo() throws Exception;

	public List<ProjectStatus> GetProjectStatusRepo() throws Exception;

	public List<LeverTechnology> GetLeverTechnologyRepo() throws Exception;

	public List<DMAIC_Phase> GetDMAIC_PhaseRepo() throws Exception;
	
	public List<ResultArea> GetResultAreaRepo() throws Exception;
}
