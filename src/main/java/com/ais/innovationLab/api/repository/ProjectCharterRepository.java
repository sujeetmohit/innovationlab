package com.ais.innovationLab.api.repository;

import java.util.List;
import org.springframework.stereotype.Repository;
import com.ais.innovationLab.api.entity.ProjectCharter;
import com.ais.innovationLab.api.entity.ProjectCharterBenefits;
import com.ais.innovationLab.api.entity.ProjectCharterDmaicphaseToll;
import com.ais.innovationLab.api.entity.ProjectCharterDocuments;
import com.ais.innovationLab.api.entity.ProjectCharterKeyStakeHolders;
import com.ais.innovationLab.api.entity.ProjectCharterResultArea;
import com.ais.innovationLab.api.entity.ProjectCharterRevisionHistory;
import com.ais.innovationLab.api.entity.ProjectCharterSteeringCommitee;
import com.ais.innovationLab.api.entity.ProjectCharterSteeringComtProjectTeam;

@Repository
public interface ProjectCharterRepository {

	public List<ProjectCharter> GetProjectCharterDetailsRepo() throws Exception;

	public List<ProjectCharter> GetProjectCharterByProjectIdRepo(int projectId) throws Exception;

	public List<ProjectCharterResultArea> GetProjectCharterResultAreaRepo(int p_ProjectId, int p_ProjectTypeId)
			throws Exception;

	public List<ProjectCharterBenefits> GetProjectCharterBenefitsRepo(int p_ProjectId, int p_ProjectTypeId)
			throws Exception;

	public List<ProjectCharterDmaicphaseToll> GetProjectCharterDMAICPhaseRepo(int p_ProjectId, int p_ProjectTypeId)
			throws Exception;

	public List<ProjectCharterSteeringComtProjectTeam> GetProjectCharterSteeringComtProjectTeamRepo(int p_ProjectId,
			int p_ProjectTypeId) throws Exception;

	public List<ProjectCharterSteeringCommitee> GetProjectCharterSteeringCommitteRepo(int p_ProjectId,
			int p_ProjectTypeId) throws Exception;

	public List<ProjectCharterKeyStakeHolders> GetProjectCharterKeyStakeHoldersRepo(int p_ProjectId,
			int p_ProjectTypeId) throws Exception;

	public List<ProjectCharterDocuments> GetProjectCharterDocumentsRepo(int p_ProjectId, int p_ProjectTypeId)
			throws Exception;

	public List<ProjectCharterRevisionHistory> GetProjectCharterRivisionHistoryRepo(int p_ProjectId,
			int p_ProjectTypeId) throws Exception;

	public int InsertProjectCharterdetailRepo(ProjectCharter projectCharter) throws Exception;

	public int UpdateProjectCharterdetailRepo(ProjectCharter projectCharter) throws Exception;

	public int InsertProjectCharterResultAreaDetailRepo(int projectId, int projectTypeId,
			ProjectCharterResultArea projectCharterResultArea) throws Exception;

	public int DeletePreviousProjectCharterDetailRepo(int projectId, int projectTypeId) throws Exception;

	public int InsertProjectCharterDmaicphaseTollRepo(int projectId, int projectTypeId,
			ProjectCharterDmaicphaseToll projectCharterDmaicphaseToll) throws Exception;

	public int InsertProjectCharterSteeringCommitteRepo(int projectId, int projectTypeId,
			ProjectCharterSteeringCommitee projectCharterSteeringCommitee) throws Exception;

	public int InsertProjectCharterSteeringCommitteTeamRepo(int projectId, int projectTypeId,
			ProjectCharterSteeringComtProjectTeam projectCharterSteeringComtProjectTeam) throws Exception;

	public int InsertProjectCharterBenefitDetailRepo(int projectId, int projectTypeId,
			ProjectCharterBenefits projectCharterBenefits) throws Exception;

	public int InsertProjectCharterKeyStakeHolderdetailRepo(int projectId, int projectTypeId,
			ProjectCharterKeyStakeHolders projectCharterKeyStakeHolders) throws Exception;

	public int InsertProjectCharterDocumentDetailRepo(int projectId, int projectTypeId,
			ProjectCharterDocuments projectCharterDocuments) throws Exception;

	public int InsertProjectCharterRevisionHistoryRepo(int projectId, int projectTypeId,
			ProjectCharterRevisionHistory projectCharterRevisionHistory) throws Exception;

	public int UpdateProjectCharterStatusRepo(int projectId, String updatedBy, String comments, int projectStatusId)
			throws Exception;

}
