package com.ais.innovationLab.api.repository;

import org.springframework.stereotype.Repository;

import com.ais.innovationLab.api.entity.FileModel;

import org.springframework.core.io.Resource;

@Repository
public interface FileRepository {

	public String uplaodIdeaSubmissionRepo(FileModel file, int ideaSubmissionId) throws Exception;

	public Integer DeleteIdeaSubmissionRepo(int ideaSubmissionId);

	public Resource DownloadIdeaSubmissionRepo(int ideaSubmissionId) throws Exception;
	
	public int UpdateReplaceIdeaSubmissionDocRepo(String FileName, int ideaSubmissionId) throws Exception;

}
