package com.ais.innovationLab.api.exceptions;


public class MalformedJwtException extends RuntimeException {
    public MalformedJwtException(String message) {
        super(message);
    }
}
