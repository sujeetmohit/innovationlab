package com.ais.innovationLab.api.exceptions;

public class UserNotActiveException extends RuntimeException {
	public UserNotActiveException(String message) {
		super(message);
	}
}
